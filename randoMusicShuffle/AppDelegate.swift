//
//  AppDelegate.swift
//  TestingiTunesLib
//
//  Created by Jay on 8/12/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    let adBannerView: GADBannerView = {
        let view = GADBannerView()
        return view
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if !sharedAppSettings.IsAppConfigured() {
            sharedAppSettings.configureApp()
        }
        UNUserNotificationCenter.current().delegate = self
        
        setupAdBannerOnRoot()
        
        return true
    }
    
    // called if app is running in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent
        notification: UNNotification, withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // show alert while app is running in foreground
        return completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //response.notification.request.content.userInfo["id"]
        //PresentSelectedAlbumById
        let albumId = response.notification.request.content.userInfo["id"] as! String
        let rootVc = window?.rootViewController as! PageViewController
        let vc = rootVc.pages[2] as! ViewController
        
        rootVc.setViewControllers([vc], direction: .forward, animated: true, completion: nil)
        rootVc.pageControl.currentPage = 2
        vc.PresentSelectedAlbumById(albumId: albumId)
        
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let rootVc = window?.rootViewController as! PageViewController
        //let vc = rootVc.pages[0] as! ViewController
        
        let image = UIImage(named: "black_main")
        let image2 = UIImage(named: "blured_main")
        
        (rootVc.view.subviews[0].subviews[0] as! UIImageView).image = sharedAppSettings.IsBackgroudVideoOn() ? image2 : image

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        /*
         let rootVc = window?.rootViewController as! PageViewController
         let vc = rootVc.pages[0] as! ViewController
         
         sharedAppSettings.validateMediaPermission() { isOn in
         if isOn {
         vc.setupEnabledInteractionView()
         } else {
         vc.setupDisabledInteractionView()
         }
         }
         */
        //  vc.setupDisabledInteractionView()
        let rootVc = window?.rootViewController as! PageViewController

       // (rootVc.view.subviews[0].subviews[0] as! UIImageView).image = nil
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
    }
    
    private func setupAdBannerOnRoot() {
        let rootVc = window?.rootViewController as! PageViewController
        GADMobileAds.sharedInstance().start(completionHandler: { status in
            rootVc.setupAdBannerView(adBannerView: self.adBannerView)
        })
    }
}

