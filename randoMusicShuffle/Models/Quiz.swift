//
//  Quiz.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/13/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation

struct Question {
    var question: String
    var answers: [Answer]
    var correctAnswerKey: String
}

struct Answer {
    var answer: String
    var isCorrect: Bool
}
