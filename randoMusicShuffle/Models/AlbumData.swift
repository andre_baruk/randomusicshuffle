//
//  AlbumData.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/15/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
//   let singleAlbum: AlbumData = AlbumData(albumId: "1", albumTitle: "Encore", releaseDate: "2004-10-16")
struct AlbumData: Codable{
    let albumPersistentId: String
    let albumArtist: String
    let albumTitle: String
    var releaseDate: String
    var albumNotFound: Bool = false
    
    mutating func updateReleaseDate(_ date: String) {
        self.releaseDate = date
    }
    
    static func encodeList(listOfAlbums: [AlbumData]) -> Data {
        var listOfAlbumsData = Data()
        do {
            let encoder = JSONEncoder()
            listOfAlbumsData = try encoder.encode(listOfAlbums)
        } catch {
            print ("Error: \(error)")
        }
        return listOfAlbumsData
    }
    
    static func decodeToList(listOfAlbumsData: Data) -> [AlbumData] {
        var listOfAlbums = [AlbumData]()
        do {
            let decoder = JSONDecoder()
            listOfAlbums = try decoder.decode([AlbumData].self, from: listOfAlbumsData)
        } catch {
            print ("Error: \(error)")
        }
        return listOfAlbums
    }
}

extension AlbumData: Equatable {
    static func == (lhs: AlbumData, rhs: AlbumData) -> Bool {
        return  lhs.albumPersistentId == rhs.albumPersistentId
    }
}

// MARK: - Album Data View Model Struct

struct AlbumDataViewModel {
    let albumPersistentId: String
    let albumArtist: String
    let albumTitle: String
    var releaseDate: String!
    var albumCover: UIImage!
    
    var searchableDescription: String {
        return String("\(albumArtist) \(albumTitle)")
    }
}
