//
//  PageViewController.swift
//  randoMusicShuffle
//
//  Created by Jay on 10/15/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var pages = [UIViewController]()
    var pageControl = UIPageControl()
    var initialPage = 0
    var backgroundView: BackgroundView!
    var avPlayerViewContainer: UIView!
    var visualEffectView: UIVisualEffectView!
    
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    
    var isVideoBackgroundOn = true
    var currentOrientation: UIDeviceOrientation!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? UIInterfaceOrientationMask.all : UIInterfaceOrientationMask.portrait
    }
    
    // MARK: - View Setup Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentOrientation = UIDevice.current.orientation
        
        self.dataSource = self
        self.delegate = self
        initialPage = 2
        
        let mainPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! ViewController
        let decadesPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DecadesVC") as! DecadesViewController
        //  let todayPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TodayVC") as! TodayViewController
        let newsPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsVC") as! NewsViewController
        let settingsPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
        
        let bonusPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "BonusNC")
        
        //  let quizPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "QuizVC")
        // let allAlbumsPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "AllAlbumsVC")
        
        self.pages.append(settingsPage)
        // self.pages.append(upcomingPage)
        self.pages.append(bonusPage)
        // self.pages.append(quizPage)
        // self.pages.append(allAlbumsPage)
        self.pages.append(mainPage)
        
        self.pages.append(newsPage)
        self.pages.append(decadesPage)
        
        setViewControllers([pages[initialPage]], direction: .forward, animated: true, completion: nil)
        
        backgroundView = BackgroundView.instanceFromNib()
        backgroundView.frame = UIScreen.main.bounds
        //backgroundView.contentMode = .scaleAspectFill
        backgroundView.subviews[0].backgroundColor = .clear
        view.insertSubview(backgroundView, at: 0)
        
        setupPageControl()
        
        if sharedAppSettings.IsBackgroudVideoOn() {
            self.setupBackgroundVideoPlayer(isSentFromToggle: false)
        }
        /// NEEDS FIX
        // self.pageControl.addTarget(self, action: #selector(self.pageControlSelectionAction(_:)), for: .touchDown)
        pageControl.isEnabled = false
        
        /// BRIEFLY BLOCKED UNTIL FIX FOR ORIENTATION CHANGE IS READY
        /// For handling rotation changes on iPads
        NotificationCenter.default.addObserver(self, selector: #selector(PageViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            pageControl.frame.origin.y = UIScreen.main.bounds.maxY - 70
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            pageControl.frame.origin.y = UIScreen.main.bounds.maxY - 120
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let avPlayer = avPlayer {
            avPlayer.play()
        }
        
        /// for when background video is turned off
        (view.subviews[0].subviews[0] as! UIImageView).image = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if visualEffectView != nil {
            visualEffectView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
    }
    
    /// code for updating UI during screen orientation change
    @objc override func rotated() {
        if avPlayerViewContainer != nil && UIDevice.current.userInterfaceIdiom == .pad && currentOrientation.isLandscape != UIDevice.current.orientation.isLandscape {
            avPlayerViewContainer.fitLayers()
            currentOrientation = UIDevice.current.orientation
            pageControl.frame.origin.y = UIScreen.main.bounds.maxY - 50
        }
    }
    
    // MARK: - Background View Methods
    
    func setupBackgroundVideoPlayer(isSentFromToggle: Bool){
        
        /// Register Video controlling events during state change
        NotificationCenter.default.addObserver(self, selector: #selector(reInitDataAfterEnteringForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(turnOffWhenEnteredBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        /// Wrapper for background video
        avPlayerViewContainer = UIView(frame: self.view.frame)
        
        /// Blur visual effect
        visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark)) as UIVisualEffectView
        visualEffectView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        visualEffectView.alpha = 0.8
        avPlayerViewContainer.addSubview(visualEffectView)
        
        // Get video for playback
        let backgroundVideoURL = Bundle.main.url(forResource:"dark1", withExtension: "m4v")
        
        
        /// Setup player view
        backgroundView.addSubview(avPlayerViewContainer)
        if isSentFromToggle{
            avPlayerViewContainer.alpha = 0
            avPlayerViewContainer.fadeIn()
        }
        
        
        avPlayer = AVPlayer(url: backgroundVideoURL!)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avPlayer.volume = 0
        avPlayer.actionAtItemEnd = .none
        // avPlayerLayer.frame = backgroundView.subviews[0].layer.bounds
        //UIScreen.main.bounds.maxY
        avPlayerLayer.frame = UIScreen.main.bounds
        avPlayerViewContainer.layer.insertSublayer(avPlayerLayer, at: 0)
        
        /// Prevents currently playing audio from stopping due to avPlayer
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.ambient)), mode: AVAudioSession.Mode.default)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)
    }
    
    public func turnOnBackgroundVideoFromToggle() {
        setupBackgroundVideoPlayer(isSentFromToggle: true)
        avPlayer.play()
    }
    
    public func turnOffBackgroundVideoFromToggle() {
        // (view.subviews[0].subviews[0] as! UIImageView).image = nil
        avPlayerViewContainer.fadeOut() { _ in
            if let avPlayer: AVPlayer = self.avPlayer {
                avPlayer.pause()
            }
            self.avPlayer = nil
            
            if let avPlayerLayer = self.avPlayerLayer {
                avPlayerLayer.removeFromSuperlayer()
            }
            self.avPlayerLayer = nil
        }
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func reInitDataAfterEnteringForeground(_ sender: Any) {
        
        // while avPlayer.rate == 0.0 {
        avPlayer.play()
        // }
    }
    
    @objc func turnOffWhenEnteredBackground(_ sender: Any) {
        
        // if avPlayer.rate != 0.0 {
        avPlayer.pause()
        // }
    }
    
    @objc func playerItemDidReachEnd(notification: Notification)
    {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero, completionHandler: nil)
        avPlayer.play()
    }
}

// MARK: - UIPageViewControllerDelegate Extension

extension PageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // return nil
        //        if viewController is ViewController {
        //            return nil
        //        } else if viewController is StatisticsViewController {
        //            return pages[0]
        //        }
        //        return pages[1]
        
        let index = pages.firstIndex(of: viewController)
        if index == 0 {
            return nil
        } else {
            return pages[index!-1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        //        if viewController is ViewController {
        //            return pages[1]
        //        } else if viewController is StatisticsViewController {
        //            return pages[2]
        //        }
        //        return nil
        
        let index = pages.firstIndex(of: viewController)
        if index == pages.count - 1 {
            return nil
        } else {
            return pages[index!+1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = pages.firstIndex(of: pageContentViewController)!
    }
    
    func setupPageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 10))
        //  pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - UIScreen.main.bounds.maxX * 0.45,width: UIScreen.main.bounds.width,height: 100))
        
        self.pageControl.numberOfPages = pages.count
        self.pageControl.currentPage = initialPage
        self.pageControl.tintColor = UIColor.white
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        self.view.addSubview(pageControl)
    }
    
    @objc func pageControlSelectionAction(_ sender: UIPageControl) {
        
        //  pageViewController(self, viewControllerAfter: pages[0])
        
        if pageControl.currentPage == 0 {
            setViewControllers([pages[1]], direction: .forward , animated: true, completion: nil)
            
        } else if pageControl.currentPage == 1 {
            setViewControllers([pages[0]], direction: .reverse , animated: true, completion: nil)
        }
    }
}

