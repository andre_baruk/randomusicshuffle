//
//  DecadesViewController.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/1/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import MediaPlayer

class DecadesViewController: UITableViewController {

    typealias AlbumStruct = (String, String)
    
    var groupedAlbums: [String : [(AlbumStruct)]]!
    var orderedGroupKeys: Array<String>!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = .clear
        
        if let headerTextView = tableView.subviews[0] as? UITextView {
            headerTextView.textAlignment = .center
            headerTextView.textColor = .white
            headerTextView.backgroundColor = .black
            headerTextView.alpha = 0.5
            headerTextView.font = UIFont.boldSystemFont(ofSize: 18)
        }
        
        if #available(iOS 11.0, *) {
           // tableView.contentInsetAdjustmentBehavior = .never
        
          //  tableView.contentInset = UIEdgeInsets(top: -14, left: 0, bottom: 0, right: 0)
        } else {
            // Fallback on earlier versions
        }
        
       // let etst = self.navigationController?.navigationBar.frame.size.height
   
        if sharedAppSettings.IsLibraryAccessPermitted() {
        createYearlyGroupings()
        } else {
            groupedAlbums = [String:[(AlbumStruct)]]()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if groupedAlbums.count > 0 {
            addTopNavBar(andOverlay: true)
        } else if !sharedAppSettings.IsLibraryAccessPermitted() {
            addLibraryIsDisabledInformation()
            //  let infoLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 50))
            // infoLabel.text = "Nothing to show"
            //  infoLabel.center = view.center
            //  view.addSubview(infoLabel)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeTopNavBar()
    }
    
    func createYearlyGroupings() {
        let orderedAlbums = MPMediaQuery.albums().collections?.sorted(by: { $0.representativeItem!.albumTitle! < $1.representativeItem!.albumTitle! })
      //  groupedAlbums = [String: [("", "")]
        orderedGroupKeys = [String]()
        
        /// groups albums by decade
        let grouped = orderedAlbums?.reduce(into: [String: [(AlbumStruct)]]()) { result, value in
            
            let decade = value.representativeItem?.value(forProperty: "year") != nil
            && value.representativeItem?.value(forProperty: "year").debugDescription.count == 14 ? value.representativeItem!.value(forProperty: "year").debugDescription.debugDescriptionYearValue.decade : ""
            
            /// album title with year - title (year)
            let albumTitle = (value.representativeItem?.albumTitle)! + " (" + value.representativeItem!.value(forProperty: "year").debugDescription.debugDescriptionYearValue + ")"
            let persistentId = value.persistentID.description
            

            return result[decade, default: []].append((albumTitle ?? "n/a", persistentId ?? "0"))
        }
        groupedAlbums = grouped?.filter({ $0.key != "" })
        orderedGroupKeys = groupedAlbums.map({$0.key}).sorted(by: { $0 < $1 })
    }
    
}


// MARK: - UITableViewDataSource methods

extension DecadesViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return groupedAlbums.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedAlbums[orderedGroupKeys[section]]!.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelView = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        if section == 0 {
            labelView.frame = CGRect(x: 0, y: 0, width: 50, height: 200)
        }
        
        let formattedDecadeText = String("The \(orderedGroupKeys[section])'s")
        
        labelView.text = formattedDecadeText
     //   returnedView.addSubview(labelView)
        labelView.textAlignment = .center
        labelView.textColor = .white
        labelView.backgroundColor = .black
        labelView.alpha = 0.5
        labelView.font = UIFont.boldSystemFont(ofSize: 16)
        return labelView
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        cell.backgroundColor = .clear
        
        cell.textLabel?.textColor = .white
        cell.textLabel?.text = groupedAlbums[orderedGroupKeys[indexPath.section]]?[indexPath.row].0
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedAlbumId = groupedAlbums[orderedGroupKeys[indexPath.section]]?[indexPath.row].1
                guard let mediaItem = MPMediaQuery.albums().collections!.filter({ $0.persistentID.description == selectedAlbumId }).first else {
            print()
            return
        }
        
        /// setup player
        let player = MPMusicPlayerController.systemMusicPlayer
        player.setQueue(with: mediaItem)
        player.play()
        
        /// open music app
        let url = URL(string: "music://")
        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
}
