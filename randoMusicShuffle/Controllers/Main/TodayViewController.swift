//
//  TodayViewController.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/2/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import FeedKit

class TodayViewController: UITableViewController {
    
    var feedItems = [RSSFeedItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RSSFeedManager.shared.getTodayInMusicFeed() { feedResult in
            if let items = feedResult?.items {
                self.feedItems.append(contentsOf: items)
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addTopNavBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeTopNavBar()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "What Happened Today In Music"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white

        cell.textLabel?.text = feedItems[indexPath.row].title
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.sizeToFit()
        
        return cell
    }
    
}
