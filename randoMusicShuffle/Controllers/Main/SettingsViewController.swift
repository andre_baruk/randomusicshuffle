//
//  SettingsViewController.swift
//  randoMusicShuffle
//
//  Created by Jay on 10/20/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var videoBackgroundSwitch: UISwitch!
    @IBOutlet weak var shuffleAnimationSwitch: UISwitch!
    
    let adBannerView: GADBannerView = {
        let view = GADBannerView()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        videoBackgroundSwitch.isOn = sharedAppSettings.IsBackgroudVideoOn()
        shuffleAnimationSwitch.isOn = sharedAppSettings.IsShuffleAnimationOn()
       // setupAdBannerView()
    }
    
    func setupAdBannerView() {
//        view.addSubview(adBannerView)
//        adBannerView.translatesAutoresizingMaskIntoConstraints = false
//        
//        adBannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
//        adBannerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
//        adBannerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
//        adBannerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        adBannerView.adUnitID = AdUnitId;
        adBannerView.rootViewController = self;
        adBannerView.load(GADRequest());
        
       // let request: GADRequest = GADRequest()
       // request.testDevices = [(kGADSimulatorID as! String)]
       // adBannerView.load(request)
    }
    
    @IBAction func backgroundVideoSwitchToggled(_ sender: UISwitch) {
        sharedAppSettings.SetIsBackgroundVideOn(isOn: sender.isOn)
        toggleBackgroundVideo(isOn: sender.isOn)
    }
    
    @IBAction func shuffleAnimationSwitchToggled(_ sender: UISwitch) {
           sharedAppSettings.SetIsShuffleAnimationOn(isOn: sender.isOn)
    }
    
    func toggleBackgroundVideo(isOn: Bool) {
        if let pageController = self.parent as? PageViewController {
            if isOn {
                pageController.turnOnBackgroundVideoFromToggle()
            }else {
                pageController.turnOffBackgroundVideoFromToggle()
            }
        }
    }
}
