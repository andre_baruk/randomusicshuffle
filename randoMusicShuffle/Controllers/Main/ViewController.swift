//
//  ViewController.swift
//  TestingiTunesLib
//
//  Created by Jay on 8/12/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import MediaPlayer
import AVFoundation
import GoogleMobileAds

class ViewController: UIViewController,MPMediaPickerControllerDelegate {
    
    @IBOutlet weak var albumTitleLabel: UITextView!
    @IBOutlet weak var albumCoverView: UIImageView!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var shuffleButton: UIButton!
    
    var chosenAlbum: MPMediaItemCollection!
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    var albumShuffleItterationCounter = 0
    var albumShuffleTimer: Timer? = nil
    var lastShuffleButtonTapTime = Date.init()
    
    var albums: [AlbumData]? {
        get {
            return AlbumDataStore.sharedInstance.getSavedAlbums()
        }
    }
    
    // MARK:- Constraint Properties
    private var sharedConstraints: [NSLayoutConstraint] = []
    private var iPhoneConstraints: [NSLayoutConstraint] = []
    private var iPadConstraints: [NSLayoutConstraint] = []
    
    // MARK: - View Setup Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
         //           NotificationManager.sharedInstance.printAllNotifications()
     //   print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last);
        
      //  var albumsLocal = AlbumDataStore.sharedInstance.getSavedAlbums()
      //  albumsLocal = albumsLocal?.filter({ $0.albumArtist != "JXY." && $0.albumNotFound })
       // print(albumsLocal!.count)
        
     //   NotificationManager.sharedInstance.printAllNotifications()
        
        setupConstraints()
        activateConstraints()

        mediaAccessValidation() { isLibraryAccessible in
            
            if AlbumDataStore.sharedInstance.didLibraryChange() && isLibraryAccessible {
                AlbumDataStore.sharedInstance.synchronizeStoreWithLibrary()
            }
            
            if let albums = AlbumDataStore.sharedInstance.getSavedAlbums(), isLibraryAccessible {
                DeezerAPICaller.sharedInstance.getAlbumReleaseDates(albums) {
                    
                    let refreshedAlbums = AlbumDataStore.sharedInstance.getSavedAlbums()
                    let albumsWithReleaseDates = refreshedAlbums!.filter({ $0.releaseDate != "" }).count
                    print("albums with release dates: \(albumsWithReleaseDates)")
                    
                    NotificationManager.sharedInstance.updateNotifications()
                }
                NotificationManager.sharedInstance.updateNotifications()
            }
        }
        
        
        
       // NotificationManager.sharedInstance.updateNotifications()
        
      //  print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last);

    //    AlbumDataStore.sharedInstance.setupFirstTimeAlbumData()
      //  let albums = AlbumDataStore.sharedInstance.getSavedAlbums()
      //  print(albums)
      //  DeezerAPICaller.sharedInstance.getTest2()
        
        //  AlbumDataStore.sharedInstance.setupAlbumTestData()
        
        
        /*
         DeezerAPICaller.sharedInstance.getAlbumReleaseDates(albums!) {
         let albums = AlbumDataStore.sharedInstance.getSavedAlbums()
         print("print albums count - \(albums?.count)")
         }
         */
        
        //self.TestAlbumMgmt()
         self.setupAlbumView()
        
        // DeezerAPICaller.sharedInstance.getTest()
        // self.testFileSave()
        /*
         
         GADMobileAds.sharedInstance().start(completionHandler: { status in
         self.setupAdBannerView()
         })
         
         
         */
     //   GADMobileAds.sharedInstance().start(completionHandler: { status in
      //  self.setupAdBannerView()
       // })
    }
    
    func setupAlbumView() {
        albumTitleLabel.text = ""
        albumTitleLabel.textAlignment = .center
       // albumTitleLabel.numberOfLines = 0
        albumTitleLabel.sizeToFit()
        albumCoverView.isHidden = true
        albumCoverView.layer.cornerRadius = 10
        
        /// Register tap gesture for album cover image view
        self.albumCoverView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(playAlbum(_:)))
        self.albumCoverView.addGestureRecognizer(gesture)
        self.view.backgroundColor = UIColor(white: 1, alpha: 0.0)
        
        /// setup animation to continue after returning to foreground
        NotificationCenter.default.addObserver(self, selector: #selector(reInitDataAfterEnteringForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    /*
     func setupAdBannerView() {
     adBannerView.delegate = self
     adBannerView.adUnitID = AdUnitId;
     adBannerView.rootViewController = self;
     adBannerView.load(GADRequest());
     
     // let request: GADRequest = GADRequest()
     // request.testDevices = [(kGADSimulatorID as! String)]
     // adBannerView.load(request)
     }
     */
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //    avPlayer.play()
        // paused = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // avPlayer.pause()
        // paused = true
    }
    
    func setupDisabledInteractionView() {

        shuffleButton.isHidden = true
        albumTitleLabel.isHidden = true
        
        addLibraryIsDisabledInformation()
    }
    
    func setupEnabledInteractionView() {
        shuffleButton.setTitle("Tap to shuffle...", for: .normal)
        shuffleButton.titleLabel?.font = UIFont.init(name: "SFProText-HeavyItalic", size: 20)
        shuffleButton.titleLabel?.numberOfLines = 1
        shuffleButton.titleLabel?.sizeToFit()
        shuffleButton.titleLabel?.textAlignment = NSTextAlignment.center
        shuffleButton.isEnabled = true
        albumTitleLabel.text = ""
        albumTitleLabel.isHidden = false
    }
    
    
    func PresentSelectedAlbumById(albumId: String) {
        let query = MPMediaQuery.albums()
        let items = query.collections
        
        if let selectedAlbum = items!.filter({ $0.persistentID.description == albumId }).first {
            albumTitleLabel.text = selectedAlbum.representativeItem!.albumTitle
            
            let cover = selectedAlbum.representativeItem?.artwork?.image(at: CGSize(width: 343, height: 343))
            
            albumCoverView.image = cover
            albumCoverView.layer.cornerRadius = 10
            albumCoverView.layer.borderWidth = 3;
            albumCoverView.isHidden = false
            
            chosenAlbum = selectedAlbum
        }
    }
    
    // MARK:- Constraint Methods
    
    private func setupConstraints() {
        albumCoverView.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints.append(contentsOf: [
            albumCoverView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            albumCoverView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        
        iPhoneConstraints.append(contentsOf: [
            albumCoverView.widthAnchor.constraint(equalToConstant: 374),
            albumCoverView.heightAnchor.constraint(equalToConstant: 374),
        ])
        
        iPadConstraints.append(contentsOf: [
            albumCoverView.widthAnchor.constraint(equalToConstant: 612),
            albumCoverView.heightAnchor.constraint(equalToConstant: 612),
        ])
        
        albumTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints.append(contentsOf: [
            albumTitleLabel.topAnchor.constraint(equalTo: albumCoverView.bottomAnchor, constant: 5),
            albumCoverView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            albumCoverView.heightAnchor.constraint(equalToConstant: 106),
        ])
        
        iPhoneConstraints.append(contentsOf: [
            albumCoverView.widthAnchor.constraint(equalToConstant: 300),
        ])
        
        iPadConstraints.append(contentsOf: [
            albumCoverView.widthAnchor.constraint(equalToConstant: 753),
        ])
        
        shuffleButton.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints.append(contentsOf: [
            shuffleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
        
        iPhoneConstraints.append(contentsOf: [
            shuffleButton.widthAnchor.constraint(equalToConstant: 303),
            shuffleButton.heightAnchor.constraint(equalToConstant: 100),
            shuffleButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
        ])
        
        iPadConstraints.append(contentsOf: [
            shuffleButton.widthAnchor.constraint(equalToConstant: 754),
            shuffleButton.heightAnchor.constraint(equalToConstant: 160),
            shuffleButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80),
        ])
    }
    
    private func activateConstraints() {
        NSLayoutConstraint.activate(sharedConstraints)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            NSLayoutConstraint.activate(iPadConstraints)
        } else {
            NSLayoutConstraint.activate(iPhoneConstraints)
        }
    }
    
    // MARK:- Timer Methods
    
    func scheduledTimerWithTimeInterval()
    {
        albumShuffleTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(updateCounting(timer:)), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(timer: Timer)
    {
        
        if sharedAppSettings.IsShuffleAnimationOn() {
            let query = MPMediaQuery.albums()
            let items = query.collections
            
            let lower = 0
            let upper = items!.count
            let randomNumber = Int(arc4random_uniform(UInt32(upper - lower)) + UInt32(lower))
            
            guard items?.count ?? 0 > 0 else {
                self.stopTimer()
                albumShuffleItterationCounter = 0;
                shuffleButton.isEnabled = true
                return
            }
            
            chosenAlbum = items![randomNumber]
            albumTitleLabel.text = chosenAlbum.representativeItem!.albumTitle
            
            let cover = chosenAlbum.representativeItem?.artwork?.image(at: CGSize(width: 343, height: 343))
            
            albumCoverView.image = cover
            albumCoverView.layer.cornerRadius = 10
            albumCoverView.layer.borderWidth = 3;
            albumCoverView.isHidden = false
            
            //NSLog(chosenAlbum.representativeItem!.albumTitle!)
            //print(albumShuffleItterationCounter)
            
            albumShuffleItterationCounter += 1
            self.albumCoverView.setNeedsDisplay()
            
        }
        
        if (albumShuffleItterationCounter == 15 || !sharedAppSettings.IsShuffleAnimationOn())
        {
            self.stopTimer()
            albumShuffleItterationCounter = 0;
            shuffleButton.isEnabled = true
        }
    }
    
    func stopTimer(){
        albumShuffleTimer?.invalidate()
    }
    
    // MARK: - Action Methods
    
    @IBAction func showAlbums(_ sender: Any) {
       // validateAuthorization() {
         //   return
       // }
        
        let query = MPMediaQuery.albums()
        let items = query.collections
        
        shuffleButton.isEnabled = false
        
        shuffleButton.titleLabel!.fadeOut()
        shuffleButton.setNeedsDisplay()
        lastShuffleButtonTapTime = Date.init()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { // change 2 to desired number of seconds
            
            let elapsedTimeSinceLastTap = Date().timeIntervalSince(self.lastShuffleButtonTapTime)
            let elapsedTimeSinceLastTap_Int = Int(elapsedTimeSinceLastTap)
            if (elapsedTimeSinceLastTap_Int >= 10)
            {
                self.shuffleButton.titleLabel!.fadeIn()
            }
        }
        
        scheduledTimerWithTimeInterval()
        
        let lower = 0
        let upper = items!.count
        let randomNumber = Int(arc4random_uniform(UInt32(upper - lower)) + UInt32(lower))
        
        guard items?.count ?? 0 > 0 else {
            let alertController = UIAlertController(title: "randoMusic", message:
                "Music Library is Empty.", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        chosenAlbum = items![randomNumber]
        albumTitleLabel.text = chosenAlbum.representativeItem!.albumTitle
        
        let cover = chosenAlbum.representativeItem?.artwork?.image(at: CGSize(width: 343, height: 343))
        
        albumCoverView.image = cover
        albumCoverView.layer.cornerRadius = 10
        albumCoverView.layer.borderWidth = 3;
        albumCoverView.isHidden = false
    }
    
    @IBAction func playAlbum(_ sender: Any) {
        
        /// setup player
        let player = MPMusicPlayerController.systemMusicPlayer
        player.setQueue(with: chosenAlbum)
        player.play()
        
        /// open music app
        let url = URL(string: "music://")
        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        
        /// reset view
        albumTitleLabel.text = ""
        chosenAlbum = nil
        albumCoverView.isHidden = true
    }
    
    // MARK: - Private Methods
    
    @objc private func reInitDataAfterEnteringForeground(_ sender: Any)
    {
        sharedAppSettings.validateMediaPermission() { isOn in
            if isOn {
              //  self.setupEnabledInteractionView()
            } else {
            //    self.setupDisabledInteractionView()
            }
        }
        
        albumTitleLabel.text? = ""
        albumCoverView.image = nil
        albumCoverView.isHidden = true
        print("MainVC Entered Foreground")
        // avPlayer.play()
        // paused = false
        shuffleButton.titleLabel?.fadeIn()
    }
    
    private func mediaAccessValidation(completionHandler: @escaping (Bool) -> ()) {
        sharedAppSettings.validateMediaPermission() { isOn in
            if isOn {
                DispatchQueue.main.async {
                    self.setupEnabledInteractionView()
                    completionHandler(true)
                }
            } else {
                DispatchQueue.main.async {
                    self.setupDisabledInteractionView()
                    completionHandler(false)
                }
            }
        }
    }
    
    /// previously validating media library access was checked here
    /*
    private func validateAuthorization(completion: (() -> Void) = {}) {
        let authoriationStatus = MPMediaLibrary.authorizationStatus()
        guard authoriationStatus == .authorized else {
            sharedAppSettings.SetIsLibraryAccessPermitted(isOn: false)
            return
        }
        sharedAppSettings.SetIsLibraryAccessPermitted(isOn: true)
    }
     */
}

// MARK: - GADBannerViewDelegate Methods
extension ViewController : GADBannerViewDelegate {
    /*
     /// Tells the delegate an ad request loaded an ad.
     func adViewDidReceiveAd(_ bannerView: GADBannerView) {
     print("adViewDidReceiveAd")
     }
     
     /// Tells the delegate an ad request failed.
     func adView(_ bannerView: GADBannerView,
     didFailToReceiveAdWithError error: GADRequestError) {
     print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
     }
     
     /// Tells the delegate that a full-screen view will be presented in response
     /// to the user clicking on an ad.
     func adViewWillPresentScreen(_ bannerView: GADBannerView) {
     print("adViewWillPresentScreen")
     }
     
     /// Tells the delegate that the full-screen view will be dismissed.
     func adViewWillDismissScreen(_ bannerView: GADBannerView) {
     print("adViewWillDismissScreen")
     }
     
     /// Tells the delegate that the full-screen view has been dismissed.
     func adViewDidDismissScreen(_ bannerView: GADBannerView) {
     print("adViewDidDismissScreen")
     }
     
     /// Tells the delegate that a user click will open another app (such as
     /// the App Store), backgrounding the current app.
     func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
     print("adViewWillLeaveApplication")
     }
     */
    
}
