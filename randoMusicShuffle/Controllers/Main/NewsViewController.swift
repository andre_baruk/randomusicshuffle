//
//  NewsViewController.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/2/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import FeedKit

class NewsViewController: UITableViewController {
    
    var feedItems = [RSSFeedItem]()
    let imageCache = NSCache<NSString, UIImage>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        
        RSSFeedManager.shared.getAllNewsFeeds() { feedResult in
            if let items = feedResult?.items {
                self.feedItems.append(contentsOf: items)
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addTopNavBar(andOverlay: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeTopNavBar()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        section == 0 ? "Music News" : ""
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return feedItems.count + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 0 : 300
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        return footer
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsCell
        
        if indexPath.section == 0 {
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.backgroundColor = .clear
            cell.headlineTextView?.text = ""
            
        } else {
            cell.tag = indexPath.section
            
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                cell.backgroundColor = UIColor(white: 0, alpha: 0.4)
                cell.headlineTextView?.textColor = .white
                cell.headlineTextView?.backgroundColor = .clear
                
            } else {
                cell.backgroundColor = UIColor(white: 0, alpha: 0.4)
                cell.headlineTextView?.textColor = .white
                cell.headlineTextView?.backgroundColor = .clear
                cell.layer.cornerRadius = 5
                cell.layer.borderWidth = 1
                cell.layer.borderColor = UIColor.white.cgColor
            }
            
            cell.headlineTextView?.text = feedItems[indexPath.section - 1].title
            //  cell.thumbnailTextView?.sizeToFit()
            
            if indexPath.section != 0 {
                cell.thumbnailImageView?.image = nil
            }
            
            setupThumbnailImageView(cell: cell, indexPath: indexPath)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let link = URL(string: feedItems[indexPath.section - 1].link!)
        UIApplication.shared.open(link!, options: [:], completionHandler: nil)
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
    
    func setupThumbnailImageView(cell: NewsCell, indexPath: IndexPath) {
        DispatchQueue.global(qos: .background).async {
            
            /// check if the image is already in the cache
            if let imageToCache = self.imageCache.object(forKey: self.feedItems[indexPath.section - 1].title! as NSString) {
                DispatchQueue.main.async {
                cell.thumbnailImageView?.image = imageToCache
                }
            }
                /// get thumbnail image asynchronously from network
            else  if let thumbnail = self.feedItems[indexPath.section - 1].media?.mediaThumbnails?[0].attributes?.url {
                let url = URL(string: thumbnail)
                let data = try? Data(contentsOf: url!)
                let img = UIImage(data: data!)
                
                //// use main queue to update UI and add image to cache
                DispatchQueue.main.async {
                    if cell.tag == indexPath.section {
                        cell.thumbnailImageView?.image = img
                        cell.thumbnailImageView?.contentMode = .scaleAspectFit
                        cell.headlineTextView?.sizeToFit()

                        self.imageCache.setObject(img!, forKey: self.feedItems[indexPath.section - 1].title! as NSString)
                    }
                }
            }
        }
    }
    
}

class NewsCell: UITableViewCell {
    

    @IBOutlet weak var headlineTextView: UITextView!
    @IBOutlet weak var thumbnailImageView: UIImageView!    
}
