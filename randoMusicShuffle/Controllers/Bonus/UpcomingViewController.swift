//
//  UpcomingViewController.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/3/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import MediaPlayer

class UpcomingViewController: UITableViewController {
    
    var albums = [AlbumDataViewModel]()
    var areNotificationsEmpty = true
    var separatorColor: UIColor? = UIColor.clear
    var emptyDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "UpcomingViewCell", bundle: nil), forCellReuseIdentifier: "upcomingCell")
        
        if sharedAppSettings.IsLibraryAccessPermitted() {
        createAlbumDataViewModels()
        } else {
            areNotificationsEmpty = true
        }
        
        title = "Upcoming Anniversaries"
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // addTopNavBar(withHeight: 44)
       // addTopNavBar(andOverlay: true)
        if areNotificationsEmpty {
            
            separatorColor = tableView.separatorColor
            
            tableView.isUserInteractionEnabled = false
            tableView.separatorColor = .clear
            
            emptyDataLabel.text = "No upcoming anniversaries"
            emptyDataLabel.center = view.center
            emptyDataLabel.textColor = .white
            emptyDataLabel.font = UIFont.boldSystemFont(ofSize: 18)
            emptyDataLabel.textAlignment = NSTextAlignment.center
            
            view.addSubview(emptyDataLabel)
         //   removeTopNavBar()
        } else {
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    //    removeTopNavBar()
    }
    
    // MARK: - Private Methods
    
    private func createAlbumDataViewModels() {
        
        NotificationManager.sharedInstance.getPendingAlbumNotifications(){ requests in
            for request in requests {
                let id = request.content.userInfo["id"] as! String
                let title = request.content.title
                let artist = request.content.userInfo["artist"] as! String
                
                let albumData = AlbumDataStore.sharedInstance.getAlbumBy(Id: id)
                
                let items = MPMediaQuery.albums().collections
                let albumArtwork = items!.filter({ $0.persistentID.description == id }).first?.representativeItem?.artwork?.image(at: CGSize(width: 125, height: 125))
                
                let albumViewModel = AlbumDataViewModel(albumPersistentId: id, albumArtist: artist, albumTitle: title, releaseDate: albumData?.releaseDate, albumCover: albumArtwork)
                
                self.albums.append(albumViewModel)
            }
            self.albums = self.albums.sorted(by: { $0.releaseDate.getFormattedMontAndDay() < $1.releaseDate.getFormattedMontAndDay() })
            self.areNotificationsEmpty = self.albums.count == 0
            
            DispatchQueue.main.async {
                if !self.areNotificationsEmpty {
                    self.emptyDataLabel.removeFromSuperview()
                    self.tableView.separatorColor = self.separatorColor
                    self.tableView.isUserInteractionEnabled = true
                }
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - UITableViewDataSource methods

extension UpcomingViewController {
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return areNotificationsEmpty ? nil : "Upcoming Anniversaries"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "upcomingCell", for: indexPath) as! UpcomingViewCell
        
     //   if (!areNotificationsEmpty) {
            let album = albums[indexPath.row]
            
            cell.backgroundColor = .clear
            cell.artistLabel?.textColor = .white
            cell.albumLabel?.textColor = .white
            cell.releaseDateLabel?.textColor = .white
            
            cell.artistLabel?.text = album.albumArtist
            cell.albumLabel?.text = album.albumTitle
            cell.releaseDateLabel?.text = album.releaseDate
            cell.artworkImageView?.image = album.albumCover
            
            tableView.separatorColor = separatorColor
       // }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let album = albums[indexPath.row]
        let selectedAlbumId = album.albumPersistentId
                guard let mediaItem = MPMediaQuery.albums().collections!.filter({ $0.persistentID.description == selectedAlbumId }).first else {
            print()
            return
        }
        
        /// setup player
        let player = MPMusicPlayerController.systemMusicPlayer
        player.setQueue(with: mediaItem)
        player.play()
        
        /// open music app
        let url = URL(string: "music://")
        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
}

// MARK: - Upcoming View Cell Class

class UpcomingViewCell: UITableViewCell {
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
}
