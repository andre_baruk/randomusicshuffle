//
//  QuizViewController.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/10/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import SRCountdownTimer

class QuizViewController: UIViewController {
    
    // MARK:- UI Elements Properties
    
    let containerView: UIView = {
        let view = UIView()
        return view
    }()
    let countdownTimer: SRCountdownTimer = {
       let timer = SRCountdownTimer()
        return timer
    }()
    let questionLabel: UILabel = {
        let label = UILabel()
        label.text = "The question is why is there not a question?!"
        label.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: true, andStandardSize: 20.0)
        label.textColor = .white
        return label
    }()
    let answer1Button: UIButton = {
        let button = UIButton()
        button.setTitle("First Answer", for: .normal)
        return button
    }()
    let answer2Button: UIButton = {
        let button = UIButton()
        button.setTitle("Second Answer", for: .normal)
        return button
    }()
    let answer3Button: UIButton = {
        let button = UIButton()
        button.setTitle("Third Answer", for: .normal)
        return button
    }()
    let answer4Button: UIButton = {
        let button = UIButton()
        button.setTitle("Fourth Answer", for: .normal)
        return button
    }()
    var answerButtonsList: [UIButton]!
    
    let startButton: UIButton = {
        let button = UIButton()
        button.setTitle("Start Quiz", for: .normal)
        button.titleLabel?.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: true, andStandardSize: 20.0)
        return button
    }()
    
    var questions = [Question]()
    var currentQuestionIndex = 0.0
    var correctAnswers = 0.0
    var answerWasCorrect = false
    var skipSummary = false
    
    var correctScore: Float {
        return currentQuestionIndex > 0 ? Float(correctAnswers / Double(questions.count)) * 100 : 0
    }
    
    private var sharedConstraints: [NSLayoutConstraint] = []
    private var iPhoneConstraints: [NSLayoutConstraint] = []
    private var iPadConstraints: [NSLayoutConstraint] = []
    
    private var countdownTopAnchorConstraint: NSLayoutConstraint!
    private var containerHeightConstraint: NSLayoutConstraint!
    
    private let isIpad = UIDevice.current.userInterfaceIdiom == .pad
    
    // MARK:- UIViewController Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        
        setupConstraints()
        setupSubviews()
        
        title = "Quiz"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        skipSummary = true
        endQuiz()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        resizeCountdownTimer()
    }
    
    // MARK:- UI Methods
    
    private func setupSubviews() {
        startButton.titleLabel?.adjustsFontSizeToFitWidth = true
        startButton.backgroundColor = .orange
        startButton.alpha = 0.5
        startButton.layer.cornerRadius = 5
        startButton.titleEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        startButton.showsTouchWhenHighlighted = true
        startButton.addTarget(self, action: #selector(QuizViewController.beginQuiz), for: .touchUpInside)
        
        countdownTimer.isHidden = true
        countdownTimer.backgroundColor = .clear
        countdownTimer.delegate = self
        
        resizeCountdownTimer()
        
        containerView.layer.borderWidth = 2
        containerView.layer.cornerRadius = 10.0
        containerView.layer.borderColor = UIColor.white.cgColor
        containerView.isHidden = true
        
        questionLabel.numberOfLines = 2
        questionLabel.adjustsFontSizeToFitWidth = true
        questionLabel.textAlignment = .center
        
        if (isIpad) {
            questionLabel.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: false, andStandardSize: 40.0)
        } else {
            questionLabel.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: false, andStandardSize: 18.0)
        }
        
        answerButtonsList = [answer1Button, answer2Button, answer3Button, answer4Button]
        
        for answerButton in answerButtonsList {
            answerButton.titleLabel?.adjustsFontSizeToFitWidth = true
            answerButton.backgroundColor = .blue
            answerButton.alpha = 0.5
            answerButton.layer.cornerRadius = 5
            answerButton.titleEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
            answerButton.showsTouchWhenHighlighted = true
            answerButton.titleLabel?.font = FontHelper.helveticaNeueMediumItalicFont(forDynamicIdioms: true, andStandardSize: 18.0)
            
            answerButton.addTarget(self, action: #selector(QuizViewController.validateAnswer(_:)), for: .touchUpInside)
        }
    }
    
    private func setupConstraints() {
        view.addSubview(startButton)
        startButton.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints = [
        startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        startButton.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ]
        
        iPhoneConstraints = [
        startButton.widthAnchor.constraint(equalToConstant: 100),
        startButton.heightAnchor.constraint(equalToConstant: 50)
            ]
        
        iPadConstraints = [
        startButton.widthAnchor.constraint(equalToConstant: 300),
        startButton.heightAnchor.constraint(equalToConstant: 80)
            ]
        
        view.addSubview(countdownTimer)
        countdownTimer.translatesAutoresizingMaskIntoConstraints = false
        countdownTimer.isHidden = false
        sharedConstraints.append(contentsOf: [
            countdownTimer.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        iPhoneConstraints.append(contentsOf: [
            countdownTimer.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            countdownTimer.widthAnchor.constraint(equalToConstant: 70),
            countdownTimer.heightAnchor.constraint(equalToConstant: 70)
        ])
        
        countdownTopAnchorConstraint = countdownTimer.topAnchor.constraint(equalTo: view.topAnchor, constant: 40)
        iPadConstraints.append(contentsOf: [
            countdownTopAnchorConstraint,
            countdownTimer.widthAnchor.constraint(equalToConstant: 100),
            countdownTimer.heightAnchor.constraint(equalToConstant: 200)
        ])
        
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints.append(contentsOf: [
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        //  containerView.topAnchor.constraint(equalTo: countdownTimer.bottomAnchor, constant: 10).isActive = true
        // containerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
            ])
        
        iPhoneConstraints.append(contentsOf: [
        containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        containerView.widthAnchor.constraint(equalToConstant: 300),
        containerView.heightAnchor.constraint(equalToConstant: 250),
            ])
        
        containerHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: 500)
        iPadConstraints.append(contentsOf: [
        containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 100),
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -150),
        containerHeightConstraint,
            ])
        
        containerView.addSubview(questionLabel)
        questionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        sharedConstraints.append(contentsOf: [
        questionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            ])
        
        iPhoneConstraints.append(contentsOf: [
        questionLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
        questionLabel.widthAnchor.constraint(equalTo: containerView.widthAnchor, constant: -10),
        questionLabel.heightAnchor.constraint(equalToConstant: 40),
            ])
        
        iPadConstraints.append(contentsOf: [
        questionLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
        questionLabel.widthAnchor.constraint(equalTo: containerView.widthAnchor, constant: -10),
        questionLabel.heightAnchor.constraint(equalToConstant: 80),
            ])
        
        /// answer #1 is top left
        containerView.addSubview(answer1Button)
        answer1Button.translatesAutoresizingMaskIntoConstraints = false
        
        
        iPhoneConstraints.append(contentsOf: [
        answer1Button.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 80),
        answer1Button.centerXAnchor.constraint(equalTo: containerView.leftAnchor, constant: 80),
        answer1Button.widthAnchor.constraint(equalToConstant: 100),
        answer1Button.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        iPadConstraints.append(contentsOf: [
        answer1Button.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 150),
        answer1Button.centerXAnchor.constraint(equalTo: containerView.leftAnchor, constant: 150),
        answer1Button.widthAnchor.constraint(equalToConstant: 200),
        answer1Button.heightAnchor.constraint(equalToConstant: 80)
            ])
        
        /// answer #2 is bottom left
        containerView.addSubview(answer2Button)
        answer2Button.translatesAutoresizingMaskIntoConstraints = false
        
        iPhoneConstraints.append(contentsOf: [
        answer2Button.centerXAnchor.constraint(equalTo: containerView.leftAnchor, constant: 80),
        answer2Button.topAnchor.constraint(equalTo: answer1Button.bottomAnchor, constant: 40),
        answer2Button.widthAnchor.constraint(equalToConstant: 100),
        answer2Button.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        iPadConstraints.append(contentsOf: [
        answer2Button.centerXAnchor.constraint(equalTo: containerView.leftAnchor, constant: 150),
        answer2Button.topAnchor.constraint(equalTo: answer1Button.bottomAnchor, constant: 80),
        answer2Button.widthAnchor.constraint(equalToConstant: 200),
        answer2Button.heightAnchor.constraint(equalToConstant: 80)
        ])
        
        /// answer #3 is top right
        containerView.addSubview(answer3Button)
        answer3Button.translatesAutoresizingMaskIntoConstraints = false
        
        iPhoneConstraints.append(contentsOf: [
        answer3Button.centerXAnchor.constraint(equalTo: containerView.rightAnchor, constant: -80),
        answer3Button.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 80),
        answer3Button.widthAnchor.constraint(equalToConstant: 100),
        answer3Button.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        iPadConstraints.append(contentsOf: [
        answer3Button.centerXAnchor.constraint(equalTo: containerView.rightAnchor, constant: -150),
        answer3Button.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 150),
        answer3Button.widthAnchor.constraint(equalToConstant: 200),
        answer3Button.heightAnchor.constraint(equalToConstant: 80)
            ])
        
        /// answer #4 is bottom right
        containerView.addSubview(answer4Button)
        answer4Button.translatesAutoresizingMaskIntoConstraints = false
        
        iPhoneConstraints.append(contentsOf: [
        answer4Button.centerXAnchor.constraint(equalTo: containerView.rightAnchor, constant: -80),
        answer4Button.topAnchor.constraint(equalTo: answer3Button.bottomAnchor, constant: 40),
        answer4Button.widthAnchor.constraint(equalToConstant: 100),
        answer4Button.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        iPadConstraints.append(contentsOf: [
        answer4Button.centerXAnchor.constraint(equalTo: containerView.rightAnchor, constant: -150),
        answer4Button.topAnchor.constraint(equalTo: answer3Button.bottomAnchor, constant: 80),
        answer4Button.widthAnchor.constraint(equalToConstant: 200),
        answer4Button.heightAnchor.constraint(equalToConstant: 80)
        ])

        activateConstraints()
    }
    
    private func activateConstraints() {
        NSLayoutConstraint.activate(sharedConstraints)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            NSLayoutConstraint.activate(iPadConstraints)
            resizeCountdownTimer()
            resizeContainerView()
        } else {
            NSLayoutConstraint.activate(iPhoneConstraints)
        }
    }

    private func resizeCountdownTimer() {
        if UIDevice.current.orientation.isPortrait {
            countdownTopAnchorConstraint.isActive = false
            countdownTopAnchorConstraint = countdownTimer.topAnchor.constraint(equalTo: view.topAnchor, constant: 100)
            countdownTopAnchorConstraint.isActive = true
            
        } else {
            countdownTopAnchorConstraint.isActive = false
            countdownTopAnchorConstraint = countdownTimer.topAnchor.constraint(equalTo: view.topAnchor, constant: 40)
            countdownTopAnchorConstraint.isActive = true
        }
    }
    
    private func resizeContainerView() {
        if UIDevice.current.orientation.isPortrait {
            containerHeightConstraint.isActive = false
            containerHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: 500)
            containerHeightConstraint.isActive = true
            
        } else {
            containerHeightConstraint.isActive = false
            containerHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: 450)
            containerHeightConstraint.isActive = true
        }
    }
    
    // MARK:- Action Methods
    
    @objc func beginQuiz() {        
        startButton.isHidden = true
        containerView.isHidden = false
        startCountdownTimer()
        
        questions = QuizQuestionsStore.shared.getShuffledQuestionsAndAnswers()
        currentQuestionIndex = 0
        correctAnswers = 0
        startNextQuestion(Int(currentQuestionIndex))
    }
    
    @objc func validateAnswer(_ sender: UIButton) {
        currentQuestionIndex = currentQuestionIndex + 1
        if sender.tag == 1 {
            answerWasCorrect = true
            correctAnswers = correctAnswers + 1
            countdownTimer.end()
            startNextQuestion(Int(currentQuestionIndex))
            countdownTimer.start(beginingValue: 10)
        } else {
            showQuizSummary()
        }
    }
    
    // MARK:- Quiz Game Methods
    
    func startNextQuestion(_ nextIndex: Int) {
        answerWasCorrect = false
        
        if nextIndex < questions.count {
            let question = questions[nextIndex]
            
            questionLabel.text = question.question
            answer1Button.setTitle(question.answers[0].answer, for: .normal)
            answer1Button.tag = question.answers[0].isCorrect ? 1 : 0
            
            answer2Button.setTitle(question.answers[1].answer, for: .normal)
            answer2Button.tag = question.answers[1].isCorrect ? 1 : 0
            
            answer3Button.setTitle(question.answers[2].answer, for: .normal)
            answer3Button.tag = question.answers[2].isCorrect ? 1 : 0
            
            answer4Button.setTitle(question.answers[3].answer, for: .normal)
            answer4Button.tag = question.answers[3].isCorrect ? 1 : 0
        } else {
            showQuizSummary()
        }
    }
    
    private func showQuizSummary() {
        containerView.isHidden = true
        startButton.isHidden = false
        countdownTimer.isHidden = true
        
        var textToDisplay = ""
            
        if correctScore < 100 {
           textToDisplay = String(format: "Your score is: %.0f%%.\n That's bad.\n Try again...", correctScore)
        } else {
            textToDisplay = String(format: "Your score is: %.0f%%.\n That is good.", correctScore)
        }
        
        let summaryAlert = UIAlertController(title: "The End", message: textToDisplay, preferredStyle: UIAlertController.Style.alert)
        summaryAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        present(summaryAlert, animated: true, completion: nil)
    }
    
    private func endQuiz() {
        countdownTimer.end()
        countdownTimer.isHidden = true
        containerView.isHidden = true
        startButton.isHidden = false
    }
    
}

// MARK:- SRCountdownTimerDelegate Methods

extension QuizViewController : SRCountdownTimerDelegate {
    public func start(beginingValue: Int, interval: TimeInterval = 1) {
        
    }
    
    @objc func timerDidEnd() {
        if !skipSummary {
        if !answerWasCorrect { showQuizSummary() }
        } else {
            skipSummary = false
        }
    }
    
    private func startCountdownTimer() {
        countdownTimer.isHidden = false
        countdownTimer.start(beginingValue: 10)
        countdownTimer.backgroundColor = .clear
        if let countdownLabel = countdownTimer.subviews[0] as? UILabel {
            countdownLabel.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: true, andStandardSize: 20.0)
            countdownLabel.textColor = .white
            
            if (isIpad) {
                countdownLabel.textAlignment = .center
                
//                if UIDevice.current.orientation.isPortrait {
//                    countdownLabel.frame = CGRect(x: 60, y: 60, width: 80, height: 80)
//                } else {
//                    countdownLabel.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: true, andStandardSize: 10.0)
//                    countdownLabel.frame = CGRect(x: 60, y: 60, width: 20, height: 20)
//                }
                
            } else {
            countdownLabel.frame = CGRect(x: 10, y: 10, width: 50, height: 50)
                        countdownLabel.textAlignment = .center
            }
        }
    }
}

extension Double {
    var idiomCustomized: CGFloat {
        return UIDevice.current.userInterfaceIdiom == .pad ? CGFloat(self * 2) : CGFloat(self)
    }
}
