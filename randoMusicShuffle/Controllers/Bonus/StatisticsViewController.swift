//
//  StatisticsViewController.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 6/30/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import MediaPlayer

class StatisticsViewController: UITableViewController {
    
    var statisticsList: Array<Any?>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isScrollEnabled = false
       // tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        tableView.allowsSelection = false
        tableView.separatorColor = .clear
        if sharedAppSettings.IsLibraryAccessPermitted() {
            setupList()
        } else {
            addLibraryIsDisabledInformation()
        }
        
       // navigationItem.titleView?.backgroundColor = .white
        title = "Statistics"
    }
    
    func setupList() {
        let songs = MPMediaQuery.songs().collections?.count
        let albums = MPMediaQuery.albums().collections?.count
        let artists = MPMediaQuery.artists().collections?.count
        let playlists = MPMediaQuery.playlists().collections?.count
        let genres = MPMediaQuery.genres().collections?.count
        let audioBooks = MPMediaQuery.audiobooks().collections?.count
        let podcasts = MPMediaQuery.podcasts().collections?.count
        let playCount = MPMediaQuery.songs().items?.map({ $0.playCount }).reduce(0, +)

        let mostPlayedAlbum = MPMediaQuery.albums().items?.max(by: { $0.playCount < $1.playCount })
        let mostPlayedSong = MPMediaQuery.songs().items?.max(by: { $0.playCount < $1.playCount })
        let mostPlayedArtist = MPMediaQuery.artists().items?.max(by: { $0.playCount < $1.playCount })
        let mostPlayedPlaylist = MPMediaQuery.playlists().items?.count ?? 0 > 0 ? MPMediaQuery.playlists().collections?.max(by: { $0.representativeItem!.playCount < $1.representativeItem!.playCount })!.value(forProperty: MPMediaPlaylistPropertyName) : "N/A"

        statisticsList = [songs, albums, artists, playlists, genres, audioBooks, podcasts, playCount, mostPlayedAlbum, mostPlayedSong, mostPlayedArtist, mostPlayedPlaylist]
    }
    
}

// MARK: - UITableViewDataSource methods

extension StatisticsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (statisticsList?.count ?? 0) + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        cell.backgroundColor = .clear
        setupCell(cell, at: indexPath.row)
        
        return cell
    }
    
    private func setupCell(_ cell: UITableViewCell, at: Int) {
        switch at {
        case 0:
            cell.textLabel?.text = "Your Music Library Statistics"
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.font = .boldSystemFont(ofSize: 18)
        case 1:
            let songsCount = (statisticsList?[0] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Song Count - \(songsCount)"
            break
        case 2:
            let albumsCount = (statisticsList?[1] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Album Count - \(albumsCount)"
            break
        case 3:
            let artistsCount = (statisticsList?[2] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Artists Count - \(artistsCount)"
            break
        case 4:
            let playlistsCount = (statisticsList?[3] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Playlists Count - \(playlistsCount)"
            break
        case 5:
            let genresCount = (statisticsList?[4] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Genres Count - \(genresCount)"
            break
        case 6:
            let audioBooksCount = (statisticsList?[5] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Audiobooks Count - \(audioBooksCount)"
            break
        case 7:
            let podcastsCount = (statisticsList?[6] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Podcasts Count - \(podcastsCount)"
            break
        case 8:
            let playCount = (statisticsList?[7] as? Int)?.description ?? "N/A"
            cell.textLabel?.text = "Overall Play Count - \(playCount)"
            break
        case 9:
            let mostPlayedAlbum = (statisticsList?[8] as? MPMediaItem)?.albumTitle ?? "N/A"
            cell.textLabel?.text = "Most Played Album Is - \(mostPlayedAlbum)"
            break
        case 10:
            let mostPlayedSong = (statisticsList?[9] as? MPMediaItem)?.title ?? "N/A"
            cell.textLabel?.text = "Most Played Song Is - \(mostPlayedSong)"
            break
        case 11:
            let mostPlayedArtist = (statisticsList?[10] as? MPMediaItem)?.albumArtist ?? "N/A"
            cell.textLabel?.text = "Most Played Artist Is - \(mostPlayedArtist)"
            break
        case 12:
            let mostPlayedPlaylist = statisticsList?[11] as? String ?? "N/A"
            cell.textLabel?.text = "Most Played Playlist Is - \(mostPlayedPlaylist)"
            break
        case 13:
            break
        default: break
            
        }
        
        cell.textLabel?.textColor = .white
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.sizeToFit()
    }
    
}
