//
//  AllAlbumsViewController.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/15/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import MediaPlayer

class AllAlbumsViewController: UITableViewController {
    
    var albumModels = [AlbumDataViewModel]()
    var filteredAlbumModels = [AlbumDataViewModel]()
    var selectedAlbumId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AllAlbumsViewCell", bundle: nil), forCellReuseIdentifier: "allAlbumsCell")
        
        if sharedAppSettings.IsLibraryAccessPermitted() {
            createAlbumDataViewModels()
        }
        setupSearchBar()
        
        title = "Edit Release Dates"
    }
    
    func setupSearchBar() {
        if let searchBar = tableView.subviews[0] as? UISearchBar {
            searchBar.delegate = self
            searchBar.barTintColor = .clear
            searchBar.alpha = 0.5
            
            let searchTextField = searchBar.value(forKey: "searchField") as? UITextField
            searchTextField?.textColor = .white
        }
        tableView.keyboardDismissMode = .onDrag
    }
    
    func createAlbumDataViewModels() {
        let albums = AlbumDataStore.sharedInstance.getSavedAlbums()
        let items = MPMediaQuery.albums().collections
        
        for album in albums!.sorted(by: { $0.albumTitle < $1.albumTitle }) {
            let albumArtwork = items!.filter({ $0.persistentID.description == album.albumPersistentId }).first?.representativeItem?.artwork?.image(at: CGSize(width: 125, height: 125))
            
            let albumViewModel = AlbumDataViewModel(albumPersistentId: album.albumPersistentId, albumArtist: album.albumArtist, albumTitle: album.albumTitle, releaseDate: album.releaseDate, albumCover: albumArtwork)
            
            albumModels.append(albumViewModel)
        }
        filteredAlbumModels = albumModels
    }
    
    @objc func updateReleaseDate(_ sender: AllAlbumsViewCellButton) {
        if var selectedAlbum = AlbumDataStore.sharedInstance.getAlbumBy(Id: sender.albumId) {
            
            let datePicker = UIDatePicker()
            datePicker.timeZone = .current
            datePicker.datePickerMode = .date
            datePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            let formattedDate = df.date(from: selectedAlbum.releaseDate)
            datePicker.setDate(formattedDate ?? Date.init(), animated: true)
            
            let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
            alertController.view.addSubview(datePicker)
            let confirmAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
                selectedAlbum.updateReleaseDate(df.string(from: datePicker.date))
                AlbumDataStore.sharedInstance.updateReleaseDates(updatedAlbums: [selectedAlbum])
                
                NotificationManager.sharedInstance.removeDeletedAlbumNotification(album: selectedAlbum)
                NotificationManager.sharedInstance.updateNotifications()
                
                let indexOfAll = self.albumModels.firstIndex(where: { $0.albumPersistentId == sender.albumId })!
                let indexOfFiltered = self.filteredAlbumModels.firstIndex(where: { $0.albumPersistentId == sender.albumId })!
                
                self.albumModels[indexOfAll].releaseDate = selectedAlbum.releaseDate
                self.filteredAlbumModels[indexOfFiltered].releaseDate = selectedAlbum.releaseDate
                self.tableView.reloadData()
                
                //                UIView.animate(withDuration: 0.5, animations: {
                //                    self.tableView.cellForRow(at: sender.tag)
                //                })
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true)
            
        }
    }
}

// MARK: - UITableViewDataSource methods

extension AllAlbumsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filteredAlbumModels.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "allAlbumsCell", for: indexPath) as! AllAlbumsViewCell
        
        //   if (!areNotificationsEmpty) {
        let album = filteredAlbumModels[indexPath.row]
        
        cell.backgroundColor = .clear
        cell.artistLabel?.textColor = .white
        cell.albumLabel?.textColor = .white
        cell.releaseDateLabel?.textColor = .white
        
        cell.albumLabel?.adjustsFontSizeToFitWidth = true
        cell.artistLabel?.adjustsFontSizeToFitWidth = true
        cell.releaseDateLabel?.adjustsFontSizeToFitWidth = true
        
        cell.artworkImageView?.layer.cornerRadius = 5.0
        
        cell.artistLabel?.text = album.albumArtist
        cell.albumLabel?.text = album.albumTitle
        cell.releaseDateLabel?.text = album.releaseDate
        cell.artworkImageView?.image = album.albumCover
        
        
        cell.updateButton.tag = indexPath.row
        cell.updateButton.albumId = album.albumPersistentId
        cell.updateButton.addTarget(self, action: #selector(updateReleaseDate(_:)), for: .touchUpInside)
        
        // tableView.separatorColor = separatorColor
        // }
        
        return cell
    }
    
    /// section indexes code
    /*
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return getIndexes()
    }
    
    private func getIndexes() -> [String] {
        var characters = albumModels.map({ $0.albumTitle.prefix(1).description.uppercased() })
        characters = Array(Set(characters)).sorted(by: { $0 < $1 })
        return Array(Set(characters))
    }
 */
}

// MARK: - UISearchController methods

extension AllAlbumsViewController : UISearchBarDelegate, UISearchControllerDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            filteredAlbumModels = albumModels.filter({ $0.searchableDescription.lowercased().contains(searchText.lowercased()) })
        } else {
            filteredAlbumModels = albumModels
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
}

// MARK: - All Albums View Cell Class

class AllAlbumsViewCell: UITableViewCell {
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var updateButton: AllAlbumsViewCellButton!
}

// MARK: - All Albums View Cell Button Class

class AllAlbumsViewCellButton: UIButton {
    var albumId: String = ""
}
