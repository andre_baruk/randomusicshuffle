//
//  BonusHubViewController.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/8/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit

class BonusHubViewController : UIViewController {
    
    // MARK:- UI Elements Properties
    
    let containerView: UIView = {
        let view = UIView()
        return view
    }()
    let upcomingButton: UIButton = {
        let button = UIButton()
        button.setTitle("Upcoming Anniversaries", for: .normal)
        return button
    }()
    let statisticsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Statistics", for: .normal)
        return button
    }()
    let quizButton: UIButton = {
        let button = UIButton()
        button.setTitle("Music Quiz", for: .normal)
        return button
    }()
    let editReleaseDatesButton: UIButton = {
        let button = UIButton()
        button.setTitle("Edit Release Dates", for: .normal)
        return button
    }()
    let musicHubLabel: UILabel = {
        let label = UILabel()
        label.text = "Music Hub"
        label.font = FontHelper.sfProHeavyItalicFont(forDynamicIdioms: true, andStandardSize: 20.0)
        label.textColor = .white
        return label
    }()
    
    var didReturnFromChild = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        
        setupNavBar()
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if didReturnFromChild {
            // UIView.animate(withDuration: 0.1, animations: {
            self.view.alpha = 1.0
            //   })
        }
    }
    
    // MARK:- UI Methods
    
    private func setupLayouts() {
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        //  containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 700).isActive = true
        
        musicHubLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        musicHubLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 150).isActive = true
        musicHubLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        musicHubLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upcomingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upcomingButton.topAnchor.constraint(equalTo: musicHubLabel.bottomAnchor, constant: 40).isActive = true
        upcomingButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        upcomingButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        quizButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        quizButton.topAnchor.constraint(equalTo: upcomingButton.bottomAnchor, constant: 20).isActive = true
        quizButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        quizButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        editReleaseDatesButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        editReleaseDatesButton.topAnchor.constraint(equalTo: quizButton.bottomAnchor, constant: 20).isActive = true
        editReleaseDatesButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        editReleaseDatesButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        statisticsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        statisticsButton.topAnchor.constraint(equalTo: editReleaseDatesButton.bottomAnchor, constant: 20).isActive = true
        statisticsButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        statisticsButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func setupSubviews() {
        containerView.addSubview(musicHubLabel)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        musicHubLabel.translatesAutoresizingMaskIntoConstraints = false
        musicHubLabel.textAlignment = .center
        
        containerView.addSubview(upcomingButton)
        
        upcomingButton.titleLabel?.adjustsFontSizeToFitWidth = true
        upcomingButton.backgroundColor = .blue
        upcomingButton.alpha = 0.5
        upcomingButton.layer.cornerRadius = 5
        upcomingButton.showsTouchWhenHighlighted = true
        upcomingButton.translatesAutoresizingMaskIntoConstraints = false
        upcomingButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 1, bottom: 5, right: 1)
        
        upcomingButton.addTarget(self, action: #selector(upcomingButtonTouched(sender:)), for: .touchUpInside)
        
        containerView.addSubview(statisticsButton)
        
        statisticsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        statisticsButton.backgroundColor = .darkGray
        statisticsButton.alpha = 0.5
        statisticsButton.layer.cornerRadius = 5
        statisticsButton.showsTouchWhenHighlighted = true
        statisticsButton.translatesAutoresizingMaskIntoConstraints = false
        // statisticsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1)
        
        statisticsButton.addTarget(self, action: #selector(statisticsButtonTouched(sender:)), for: .touchUpInside)
        
        containerView.addSubview(editReleaseDatesButton)
        
        editReleaseDatesButton.titleLabel?.adjustsFontSizeToFitWidth = true
        editReleaseDatesButton.backgroundColor = .systemGreen
        editReleaseDatesButton.alpha = 0.5
        editReleaseDatesButton.layer.cornerRadius = 5
        editReleaseDatesButton.showsTouchWhenHighlighted = true
        editReleaseDatesButton.translatesAutoresizingMaskIntoConstraints = false
        
        editReleaseDatesButton.addTarget(self, action: #selector(editReleaseDatesButtonTouched(sender:)), for: .touchUpInside)

        
        containerView.addSubview(quizButton)
        
        quizButton.titleLabel?.adjustsFontSizeToFitWidth = true
        quizButton.backgroundColor = .red
        quizButton.alpha = 0.5
        quizButton.layer.cornerRadius = 5
        quizButton.showsTouchWhenHighlighted = true
        quizButton.translatesAutoresizingMaskIntoConstraints = false
        
        quizButton.addTarget(self, action: #selector(quizButtonTouched(sender:)), for: .touchUpInside)
        
        view.addSubview(containerView)
        
    }
    
    private func setupNavBar() {
        if let navBar = navigationController?.navigationBar {
            
            navBar.backgroundColor = .clear
            // navBar.tintColor = .clear
            navBar.barTintColor = .clear
            navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            /// Previous setup with a custom navbar
            /*
             /// first remove the initial navbar which is uncustomizable
             navBar.removeFromSuperview()
             
             /// second create a custom navbar and setup it's visuals
             let customNavBar = UINavigationBar(frame: CGRect(x: navBar.bounds.origin.x, y: navBar.bounds.origin.y, width: navBar.frame.width, height: 200))
             //  customNavBar.tintColor = .clear
             //  customNavBar.backgroundColor = .clear
             //  customNavBar.barTintColor = .clear
             
             customNavBar.delegate = self
             
             /// third add new custom navbar to navigation control
             navigationController!.view.addSubview(customNavBar)
             */
        }
    }
    
    func pushViewWithTransition(controller: UIViewController) {
        UIView.animate(withDuration: 0, animations: {
            self.view.alpha = 0
        }) { _ in
            self.navigationController?.pushViewController(controller, animated: true)
            self.didReturnFromChild = true
        }
    }
    
    
    //MARK:- Action Methods
    
    @objc private func backButtonPressed() {
        //  dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func upcomingButtonTouched(sender: UIButton) {
        let upcomingPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "UpcomingVC") as! UpcomingViewController
        
        pushViewWithTransition(controller: upcomingPage)
    }
    
    @objc private func statisticsButtonTouched(sender: UIButton) {
        let statisticsPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "StatisticsVC") as! StatisticsViewController
        
        pushViewWithTransition(controller: statisticsPage)
    }
    
    @objc private func editReleaseDatesButtonTouched(sender: UIButton) {
        let allAlbumsPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "AllAlbumsVC") as! AllAlbumsViewController
        
        pushViewWithTransition(controller: allAlbumsPage)
    }
    
    @objc private func quizButtonTouched(sender: UIButton) {
        let quizPage = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "QuizVC") as! QuizViewController
        
        pushViewWithTransition(controller: quizPage)
    }

}
