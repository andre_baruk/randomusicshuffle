//
//  Extensions.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/24/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        return dateFormatter.string(from: self)
    }
    
    var nextMonth: String {
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        return nextMonth!.month
    }
    
    var year: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: self)
    }
    
    var dayNumber: Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        return Int(dateFormatter.string(from: self))!
    }
}

extension String {
    
    var decade: String {
        let index1 = self.index(self.startIndex, offsetBy: 0)
        let index2 = self.index(self.startIndex, offsetBy: 2)
        return String("\(self[index1...index2])0")
    }
    
    var debugDescriptionYearValue: String {
        if self.count == 14 {
            let index1 = self.index(self.endIndex, offsetBy: -5)
            let index2 = self.index(self.endIndex, offsetBy: -1)
            return String(self[index1..<index2])
        }
        return ""
    }
    
    func getFormattedMonth() -> String {
        let index1 = self.index(self.startIndex, offsetBy: 5)
        let index2 = self.index(self.startIndex, offsetBy: 6)
        let substring = self[index1...index2]
        return String(substring)
    }
    
    func getFormattedMonthNumber() -> Int {
        let index1 = self.index(self.startIndex, offsetBy: 5)
        let index2 = self.index(self.startIndex, offsetBy: 6)
        let substring = self[index1...index2]
        return Int(String(substring)) ?? 0
    }
    
    func getFormattedYear() -> String {
        let index1 = self.index(self.startIndex, offsetBy: 0)
        let index2 = self.index(self.startIndex, offsetBy: 3)
        let substring = self[index1...index2]
        return String(substring)
    }
    
    func getFormattedYearNumber() -> Int {
        let index1 = self.index(self.startIndex, offsetBy: 0)
        let index2 = self.index(self.startIndex, offsetBy: 3)
        let substring = self[index1...index2]
        return Int(String(substring)) ?? 0000
    }
    
    func getFormattedNextMonth() -> String {
        let monthInt = Int(self) ?? -1
        return String("0\(monthInt + 1)")
    }
    
    func getFormattedDayNumber() -> Int {
        let index1 = self.index(self.startIndex, offsetBy: 8)
        let index2 = self.index(self.startIndex, offsetBy: 9)
        let substring = self[index1...index2]
        return Int(String(substring)) ?? 0
    }
    
    /// mm-dd
    func getFormattedMontAndDay() -> String {
        let index1 = self.index(self.startIndex, offsetBy: 5)
        let index2 = self.index(self.endIndex, offsetBy: -1)
        let substring = self[index1...index2]
        return String(substring)
    }
}

extension Array where Element: Equatable {
mutating func removeObject(object: Element)  {
    if let index = firstIndex(of: object) {
        remove(at: index)
    }
}}
