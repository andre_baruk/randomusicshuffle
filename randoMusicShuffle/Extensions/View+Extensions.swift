//
//  View+Extensions.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/17/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import MediaPlayer

// MARK: - UIView Fade Methods Extension

extension UIView {
    func fadeIn(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 1.1, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

extension MPMediaItem {
    func albumTitleWithArtist(completion: (Bool) -> Void = {(finished: Bool) -> Void in}) -> String
    {
        if let albumArtist = self.albumArtist, let albumTitle = self.albumTitle
        {
            return albumArtist + " - " + albumTitle
        }
        return "";
        
    }
}

// MARK: - BackgroundView Class

class BackgroundView: UIView {
    class func instanceFromNib() -> BackgroundView {
        return UINib(nibName: "BackgroundView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BackgroundView
    }
}

// MARK: - Layer Fitting Methods

extension UIView {
  func fitLayers() {
    layer.fit(rect: UIScreen.main.bounds)
  }
}

extension CALayer {
  func fit(rect: CGRect) {
    frame = rect

    sublayers?.forEach { $0.fit(rect: rect) }
  }
}


// MARK: - Private Methods

// Helper function inserted by Swift 4.2 migrator.
 func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
 func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
