//
//  ViewController+Extensions.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/4/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit
import GoogleMobileAds
//[[UINavigationBar appearance] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin];

extension UIViewController: UINavigationBarDelegate {
    
    func addTopNavBar(withHeight height: Int = Int(UIApplication.shared.statusBarFrame.height), andOverlay overlay: Bool = false) {
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: height))
        
        navigationBar.backgroundColor = UIColor.black
        navigationBar.tintColor = .black
        navigationBar.barTintColor = .black
        navigationBar.autoresizingMask = .init(arrayLiteral: .flexibleWidth, .flexibleBottomMargin, .flexibleRightMargin)
        navigationBar.shadowImage = UIImage()
        navigationBar.delegate = self;
        
        if overlay {
           // self.parent?.view.subviews[0].addSubview(navigationBar)
            UIApplication.shared.keyWindow?.addSubview(navigationBar)
        } else {
            self.parent?.view.addSubview(navigationBar)
        }
        
    }
    
    func registerRotationListener() {
        /// For handling rotation changes on iPads
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    /// code for updating UI during screen orientation change
      @objc func rotated() {
        fatalError("implement custom rotated")
    }
    
    func removeTopNavBar() {
        if let navigationView = parent!.view.subviews.filter({ $0 is UINavigationBar }).first {
            navigationView.removeFromSuperview()
        } else if let overlayedNagivationView = UIApplication.shared.keyWindow?.subviews.filter({ $0 is UINavigationBar }).first {
            overlayedNagivationView.removeFromSuperview()
        }
    }
    
    func addLibraryIsDisabledInformation() {
        let informationLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width-50, height: 100))
        informationLabel.text = "Please go to settings and allow the app to access your media library in order to use this function. Reset app afterwards."
        informationLabel.font = UIFont.systemFont(ofSize: 14)
        informationLabel.textColor = .white
        
        /// centerize label in the middle of screen
        informationLabel.numberOfLines = 0
        informationLabel.sizeToFit()
        informationLabel.textAlignment = NSTextAlignment.center
        informationLabel.center = view.center
        
        self.view.addSubview(informationLabel)
        
        let goToSettingsButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
        goToSettingsButton.backgroundColor = .clear
        goToSettingsButton.setTitle("Go To App Settings", for: .normal)
        goToSettingsButton.titleLabel?.font = UIFont.init(name: "SFProText-HeavyItalic", size: 18)
        
        /// centerize horizontally and uner information label
        goToSettingsButton.sizeToFit()
        goToSettingsButton.center.x = view.center.x
        goToSettingsButton.center.y = informationLabel.center.y + 50

        goToSettingsButton.addTarget(self, action:#selector(self.goToSettings), for: .touchUpInside)
        self.view.addSubview(goToSettingsButton)
    }
    
    @objc func goToSettings() {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
    func setupAdBannerView(adBannerView: GADBannerView) {
        
        let subviewCount = view.subviews.count
        
        if subviewCount > 1 {
            view.subviews[1].addSubview(adBannerView)
        } else {
            view.addSubview(adBannerView)
        }
        
        adBannerView.translatesAutoresizingMaskIntoConstraints = false
        
        adBannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        adBannerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        adBannerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            adBannerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        } else {
            adBannerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        }
        
        
        adBannerView.adUnitID = AdUnitId
        adBannerView.rootViewController = self
        adBannerView.load(GADRequest())
    }
}
