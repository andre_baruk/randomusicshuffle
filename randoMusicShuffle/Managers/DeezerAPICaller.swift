//
//  DeezerAPICaller.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/15/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import BackgroundTasks

class DeezerAPICaller : NSObject {

    static let sharedInstance : DeezerAPICaller = {
        let instance = DeezerAPICaller()
        return instance
    }()
    
    private let deezerSearchURL = "https://api.deezer.com/"
    private var backgroundTaskID: UIBackgroundTaskIdentifier!
    
    func getAlbumReleaseDates(_ albums: [AlbumData], completion: (() -> Void)? = nil) {
        var albums = albums
        
        /// create group and  asynchronous code
        let group = DispatchGroup()
        DispatchQueue.global(qos: .background).async {
            
            /// Background tasks allow data to be loaded when app is in the background
            self.backgroundTaskID = UIApplication.shared.beginBackgroundTask (withName: "Finish Network Tasks") {
               // End the task if time expires.
               UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
                self.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
            }
            
            albums = albums.filter({ !$0.albumNotFound  && $0.releaseDate == ""  })
            var remainingAlbumsCount = albums.filter({ !$0.albumNotFound  && $0.releaseDate == ""  }).count
            
            /// split up bigger album list
            var array = [ArraySlice<AlbumData>]()
            while (remainingAlbumsCount > 0) {
                
                var partialAlbums = ArraySlice<AlbumData>()
                if remainingAlbumsCount >= 15 {
                    partialAlbums = albums[0...14]
                } else if remainingAlbumsCount > 0 && remainingAlbumsCount < 15 {
                    partialAlbums = albums[0...remainingAlbumsCount-1]
                }
                albums = albums.filter({ !partialAlbums.contains($0) })
                array.append(partialAlbums)
                remainingAlbumsCount = albums.count
            }
            
            /// itterate split album array + call get release dates and wait for batch to complete
            for partialAlbums in array {
                group.enter()
                self.getSplitAlbumReleaseDates(Array(partialAlbums)) {
                    group.leave()
                }
                group.wait()
                sleep(5)
            }
            completion?()
            
            /// end background task operation
            UIApplication.shared.endBackgroundTask(self.backgroundTaskID!)
            self.backgroundTaskID = UIBackgroundTaskIdentifier.invalid
        }
    }
    
    /// albums should be limited to ~40 per single call
    private func getSplitAlbumReleaseDates(_ albums: [AlbumData], completion: (() -> Void)? = nil) {
        var newAlbums = [AlbumData]()
        let group = DispatchGroup()
        
        for var album in albums {
            group.enter()
            self.getAlbumReleaseDate(albumData: album, completion: { json in
                if let jsonNewData = json {
                    album.releaseDate = self.getReleaseDate(json: jsonNewData)
                    album.albumNotFound = album.releaseDate.count == 0
                    newAlbums.append(album)
                } else {
                    album.albumNotFound = true
                    newAlbums.append(album)
                }
              //  print("newAlbums Count - \(newAlbums.count) + albums count - \(albums.count)")
               // if (album == albums.last) {
                    //   AlbumDataStore.sharedInstance.updateReleaseDates(updatedAlbums: newAlbums)
                    
               // }
                group.leave()
            })
        }
        group.notify(queue: .main) {
            AlbumDataStore.sharedInstance.updateReleaseDates(updatedAlbums: newAlbums)
            completion?()
        }
    }
    
    private func getReleaseDate(json: JSON) -> String {
        return json["release_date"].stringValue
    }
    
    func getAlbumReleaseDate(albumData: AlbumData, completion: ((JSON?) -> Void)?) {
        let searchByAlbumTitleURLSuffix = "search/album?q="
        let fullURL = (deezerSearchURL + searchByAlbumTitleURLSuffix + albumData.albumTitle).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!.description
        
        AF.request(fullURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            print(fullURL)
            // print(JSON(response.data)["data"].array)
            // print(JSON(response.data))
            
            /// runs through json array to check if artist name is correct to get the right album
            var properJsonEntry: JSON? = nil
            if let dataArray = JSON(response.data)["data"].array {
                for singleJsonEntry in dataArray {
                    // print(singleJsonEntry["artist"]["name"])
                    if albumData.albumArtist == singleJsonEntry["artist"]["name"].description {
                        properJsonEntry = singleJsonEntry
                        break
                    }
                }
            }
            
            if properJsonEntry == nil {
                properJsonEntry = JSON(response.data)["data"][0]
            }
            
            if let data = properJsonEntry {
                if data.count > 0, let id = data.dictionary?["id"]?.stringValue {
                    self.getAlbumDetails(albumId: id, completion: { detailsJson in
                        completion?(detailsJson)
                    })
                } else {
                    completion?(nil)
                }
            } else {
                completion?(nil)
            }
            
            /*
            if let data = response.data, let json = JSON(data)["data"].array {
                if json.count > 0, let id = json[0].dictionary?["id"]?.stringValue {
                    self.getAlbumDetails(albumId: id, completion: { detailsJson in
                        completion?(detailsJson)
                    })
                } else {
                    completion?(nil)
                }
            } else {
                completion?(nil)
            }
            */
        }
    }
    
    func getAlbumDetails(albumId: String, completion: ((JSON?) -> Void)?) {
        let albumDetailsURLSuffix = "album/"
        let fullURL = deezerSearchURL + albumDetailsURLSuffix + albumId
        // let fullURL = deezerSearchURL + albumDetailsURLSuffix + "14717564"
        AF.request(fullURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { response in
   
            switch response.result {
            case .success(let data):
                //  print("\nResponseData from getDetails:")
          //      print(JSON(data))
                
                let firstTrack = JSON(data)["tracks"]["data"][0]["id"].description
                if firstTrack.count > 0 {
                    self.getAlbumTrackDetails(trackId: firstTrack) { detailsJson in
                        completion?(detailsJson?["album"])
                    }
                } else {
                    completion?(JSON(data))
                }
                
                
            case .failure(let error):
                print("ERROR: \(String(describing: response.response?.statusCode)) + \(error)")
                completion?(nil)
            }
        }
    }
    
     func getAlbumTrackDetails(trackId: String, completion: ((JSON?) -> Void)?) {
         let albumDetailsURLSuffix = "track/"
         let fullURL = deezerSearchURL + albumDetailsURLSuffix + trackId
         // let fullURL = deezerSearchURL + albumDetailsURLSuffix + "14717564"
         AF.request(fullURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { response in
    
             switch response.result {
             case .success(let data):
               //  print("\nResponseData from getDetails:")
               //  print(JSON(data))
                 completion?(JSON(data))
             case .failure(let error):
                 print("ERROR: \(String(describing: response.response?.statusCode)) + \(error)")
                 completion?(nil)
             }

         }
     }
}
