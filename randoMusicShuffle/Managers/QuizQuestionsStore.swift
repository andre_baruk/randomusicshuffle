//
//  QuizQuestionsStore.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/13/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuizQuestionsStore {
    
    static let shared: QuizQuestionsStore = {
        let instance = QuizQuestionsStore()
        return instance
    }()
    
    let quizFileURL = Bundle.main.url(forResource:"quiz", withExtension: "json")!
    
    func getShuffledQuestionsAndAnswers() -> [Question] {
        let questions = getQuestions().shuffled()
        var questionsNew = [Question]()
        for q in questions {
            let newQ = Question(question: q.question, answers: q.answers.shuffled(), correctAnswerKey: q.correctAnswerKey)
            questionsNew.append(newQ)
        }
        return questionsNew
    }
    
    func getQuestions() -> [Question] {
        do {
            let jsonData = try JSON(Data(contentsOf: quizFileURL))
            return parseFile(jsonData: jsonData)
        } catch { }
        
        return [Question]()
    }
    
    private func parseFile(jsonData: JSON) -> [Question] {
        var questionsArray = [Question]()
        for q in jsonData["questions"].dictionaryValue {
            let answers = parseAnswers(jsonAnswers: q.value["answers"].dictionaryValue, correctAnswerKey: q.value["answer"].stringValue)
            let question = Question(question: q.key, answers: answers, correctAnswerKey: q.value["answer"].stringValue)
            questionsArray.append(question)
        }
        return questionsArray
    }
    
    private func parseAnswers(jsonAnswers: [String : JSON], correctAnswerKey: String) -> [Answer] {
        var answers = [Answer]()
        answers.append(Answer(answer: jsonAnswers["answer1"]!.stringValue, isCorrect: correctAnswerKey == "answer1"))
        answers.append(Answer(answer: jsonAnswers["answer2"]!.stringValue, isCorrect: correctAnswerKey == "answer2"))
        answers.append(Answer(answer: jsonAnswers["answer3"]!.stringValue, isCorrect: correctAnswerKey == "answer3"))
        answers.append(Answer(answer: jsonAnswers["answer4"]!.stringValue, isCorrect: correctAnswerKey == "answer4"))
        return answers
    }
}
