//
//  AlbumDataStore.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/17/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import MediaPlayer

class AlbumDataStore {
    
    static let sharedInstance : AlbumDataStore = {
        let instance = AlbumDataStore()
        return instance
    }()
    
    private let fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("album-dates")
    
    var fileExists: Bool {
        get {
            FileManager.default.fileExists(atPath: fileURL.relativePath)
        }
    }
    
    private var albums: [AlbumData]? = nil
    
    
    func getSavedAlbums() -> [AlbumData]? {
        
        if (self.albums == nil) {
            do {
                let savedData = try Data(contentsOf: self.fileURL as URL)
                let decoder = JSONDecoder()
                self.albums = try decoder.decode([AlbumData].self, from: savedData)
                return self.albums
            } catch {
                print("error getting saved album: \(error)")
                return nil
            }
        }
        return albums
    }
    
    /// will replace the WHOLE album library
    func setSavedAlbums(albums: [AlbumData]) {
        
        /// clear albums and replace with passed values
        self.albums = [AlbumData]()
        for album in albums {
            self.albums?.append(album)
        }
        
        /// encode and save to file
        var listOfAlbumsData = Data()
        do {
            let encoder = JSONEncoder()
            listOfAlbumsData = try encoder.encode(self.albums)
            /// Save to file
           let did = (listOfAlbumsData as NSData).write(to: self.fileURL as URL, atomically: true)
            print(did)
        } catch {
            print("error saving album: \(error)")
            self.albums = nil
        }
    }
    
    func setupFirstTimeAlbumData() {
        
        firstTimeParseFromMediaQueryToAlbumData()
        if let albums = self.albums, self.albums!.count > 0 {
            setSavedAlbums(albums: albums)
        }
    }
    
    func updateReleaseDates(updatedAlbums: [AlbumData]) {
        for updatedAlbum in updatedAlbums {
            if (self.albums?.filter({ $0.albumPersistentId == updatedAlbum.albumPersistentId }).first) != nil {
                let index = albums?.firstIndex(where: { $0.albumPersistentId == updatedAlbum.albumPersistentId  })
                albums?[index!].releaseDate = updatedAlbum.releaseDate
                albums?[index!].albumNotFound = updatedAlbum.albumNotFound
            }
        }
        updateSavedAlbums()
    }

    
    /// will replace the WHOLE album library by updating current private album
    private func updateSavedAlbums() {
        
        var listOfAlbumsData = Data()
        do {
            let encoder = JSONEncoder()
            listOfAlbumsData = try encoder.encode(self.albums)
            /// Save to file
           let did = (listOfAlbumsData as NSData).write(to: self.fileURL as URL, atomically: true)
            print(did)
            self.albums = nil
            self.albums = self.getSavedAlbums()
        } catch {
            print("error saving album: \(error)")
            self.albums = nil
        }
    }
    
    func getAlbumBy(Id: String) -> AlbumData? {
        if let album = getSavedAlbums()?.filter({ $0.albumPersistentId == Id }).first {
            return album
        }
        return nil
    }
    
    func didLibraryChange() -> Bool {
        sharedAppSettings.LibraryLastDateModified() != getLibraryLastModifiedDate().description
    }
    
    /// this functions synchronizes data between the user's music library
    /// and the local data store on the app
    func synchronizeStoreWithLibrary() {
        var updatedAlbums = self.getSavedAlbums()
        let albumsFromLibrary = parseFromMediaQuery()
        
        let albumsFromLibraryIdList = albumsFromLibrary.map({ $0.albumPersistentId })
        
        for albumInLocalStore in updatedAlbums! {
            if !albumsFromLibraryIdList.contains(albumInLocalStore.albumPersistentId) {
                updatedAlbums?.removeObject(object: albumInLocalStore)
                        NotificationManager.sharedInstance.removeDeletedAlbumNotification(album: albumInLocalStore)
            }
        }
        
        let albumsFromStoreIdList = updatedAlbums?.map({ $0.albumPersistentId })
        
        for albumInLibrary in albumsFromLibrary {
            if !albumsFromStoreIdList!.contains(albumInLibrary.albumPersistentId) {
                updatedAlbums?.append(albumInLibrary)
            }
        }
        self.albums = updatedAlbums
        self.updateSavedAlbums()
        
        sharedAppSettings.SetLibraryLastDateModified(date: getLibraryLastModifiedDate())
    }
    
    private func parseFromMediaQuery() -> [AlbumData] {
        
        /// get albums from device query
        let albumCollection = MPMediaQuery.albums().collections
        
        var newAlbumSet = [AlbumData]()
        
        /// loop data from media collection and add it to albums property
        for album in albumCollection!.map({ $0.representativeItem! }) {
    
            let title = album.albumTitle!
            let albumPersistentId = album.albumPersistentID.description
            let artist = album.albumArtist ?? album.artist ?? ""
            let singleAlbumData = AlbumData(albumPersistentId: albumPersistentId, albumArtist: artist, albumTitle: title, releaseDate: "")
            
            newAlbumSet.append(singleAlbumData)
        }
        return newAlbumSet
    }
    
    private func firstTimeParseFromMediaQueryToAlbumData() {
        
        /// get albums from device query
        let albumCollection = MPMediaQuery.albums().collections
        
        /// reset the album data
        self.albums = [AlbumData]()
        
        /// loop data from media collection and add it to albums property
        for album in albumCollection!.map({ $0.representativeItem! }) {
    
            let title = album.albumTitle!
            let albumPersistentId = album.albumPersistentID.description
            let artist = album.albumArtist ?? album.artist ?? ""
            let singleAlbumData = AlbumData(albumPersistentId: albumPersistentId, albumArtist: artist, albumTitle: title, releaseDate: "")
            
            albums!.append(singleAlbumData)
        }
    }
    
    private func getLibraryLastModifiedDate() -> Date {
      return MPMediaLibrary.default().lastModifiedDate
    }
}

