//
//  AppSettings.swift
//  randoMusicShuffle
//
//  Created by Jay on 10/20/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Foundation
import MediaPlayer

let sharedAppSettings = AppSettings()

//#if DEBUG
//public let AdUnitId = "ca-app-pub-3940256099942544/2934735716"
//#else
public let AdUnitId = "ca-app-pub-9875864268545161/9333854640"
//#endif

class AppSettings {
    
    private let Is_Configured = "IsConfigured"
    private let Background_Video_On_Key = "BackgroundOn"
    private let Shuffle_Animation_On_Key = "ShuffleAnimationOn"
    private let Library_Last_Date_Modified = "LibraryLastDateModified"
    private let Is_Library_Access_Permitted = "LibraryAccessPermitted"
    
   // init() { }
    
    func configureApp() {
        SetIsBackgroundVideOn(isOn: true)
        SetIsShuffleAnimationOn(isOn: true)
        askPermissions()
    }
    
    func IsAppConfigured() -> Bool {
        return UserDefaults.standard.bool(forKey: Is_Configured)
    }
    
    func SetIsAppConfigured(isOn: Bool) {
        UserDefaults.standard.set(isOn, forKey: Is_Configured)
        UserDefaults.standard.synchronize()
    }
    
     func IsBackgroudVideoOn() -> Bool {
        return UserDefaults.standard.bool(forKey: Background_Video_On_Key)
    }
    
     func SetIsBackgroundVideOn(isOn: Bool) {
        UserDefaults.standard.set(isOn, forKey: Background_Video_On_Key)
        UserDefaults.standard.synchronize()
    }
    
     func IsShuffleAnimationOn() -> Bool {
        return UserDefaults.standard.bool(forKey: Shuffle_Animation_On_Key)
    }
    
    func SetIsShuffleAnimationOn(isOn: Bool) {
        UserDefaults.standard.set(isOn, forKey: Shuffle_Animation_On_Key)
        UserDefaults.standard.synchronize()
    }
    
    func LibraryLastDateModified() -> String {
        return UserDefaults.standard.string(forKey: Library_Last_Date_Modified) ?? ""
    }
    
    func SetLibraryLastDateModified(date: Date) {
        UserDefaults.standard.set(date.description, forKey: Library_Last_Date_Modified)
        UserDefaults.standard.synchronize()
    }
    
    func SetIsLibraryAccessPermitted(isOn: Bool) {
          UserDefaults.standard.set(isOn, forKey: Is_Library_Access_Permitted)
          UserDefaults.standard.synchronize()
      }
    
    func IsLibraryAccessPermitted() -> Bool {
        return UserDefaults.standard.bool(forKey: Is_Library_Access_Permitted)
    }
    
    func validateMediaPermission(completionHandler: @escaping (Bool) -> () = { _ in  }) {
        let authoriationStatus = MPMediaLibrary.authorizationStatus()
        guard authoriationStatus == .authorized else {
            MPMediaLibrary.requestAuthorization() { status in
                if status == .authorized {
                    self.SetIsLibraryAccessPermitted(isOn: true)
                    
                    if (!AlbumDataStore.sharedInstance.fileExists) {
                        AlbumDataStore.sharedInstance.setupFirstTimeAlbumData()
                    }
                    
                    completionHandler(true)
                    return
                } else {
                    self.SetIsLibraryAccessPermitted(isOn: false)
                    completionHandler(false)
                }
             //   completionHandler(self.IsLibraryAccessPermitted())
            }
          //  completionHandler(self.IsLibraryAccessPermitted())
            return
        }
        if (!AlbumDataStore.sharedInstance.fileExists) {
            AlbumDataStore.sharedInstance.setupFirstTimeAlbumData()
        }
        self.SetIsLibraryAccessPermitted(isOn: true)
        completionHandler(self.IsLibraryAccessPermitted())
    }
    
    private func askPermissions() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            self.validateMediaPermission()
        }
        
        self.SetIsAppConfigured(isOn: true)
    }
}
