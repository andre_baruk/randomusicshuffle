//
//  RSSFeedManager.swift
//  randoMusicShuffle
//
//  Created by Andrew Baruk on 7/2/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import FeedKit
import Alamofire

class RSSFeedManager {
    
    static let shared : RSSFeedManager = {
        let instance = RSSFeedManager()
        return instance
    }()
    
    func getAllNewsFeeds(completionHandler: @escaping (RSSFeed?)->Void) {
        let feedList = [URL(string: "http://loudwire.com/feed")!, URL(string: "http://pitchfork.com/rss/news")!]
        
        for feedURL in feedList {
            let parser = FeedParser(URL: feedURL)
            self.getFeed(parser: parser, completionHandler: { feedResults in
                completionHandler(feedResults)
            })
        }
    }
    
    func getTodayInMusicFeed(completionHandler: @escaping (RSSFeed?)->Void) {
        let feedUrl = "http://www.thisdayinmusic.com/feed"
        
        /// This particular feed returns a file instead of xml.
        /// In order to correctly parse it, it must first be downloaded to 'data' type.
        AF.download(feedUrl).responseData(completionHandler: { response in
            if let file = response.value {
                
                /// The feed/file has incorrect values at the beginning of
                /// the file so it must be removed to be parsed correctly.
                let parser = FeedParser(data: file[1...file.count-1])
                self.getFeed(parser: parser, completionHandler: { feedResult in
                    completionHandler(feedResult)
                })
                
            }
        })
    }
    
    func getFeed(parser: FeedParser, completionHandler: @escaping (RSSFeed?)->Void) {
        
        // Parse asynchronously, not to block the UI.
        parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
        
            DispatchQueue.main.async {
                switch result {
                case .success(let feed):
                    completionHandler(feed.rssFeed)
                case .failure(let error):
                    print(error)
                    completionHandler(nil)
                }
            }
            
        }
    }
    
    
    func test(completionHandler: @escaping (RSSFeed)->Void) {
        let feedURL = URL(string: "http://loudwire.com/feed")!
        let parser = FeedParser(URL: feedURL)
        
        // Parse asynchronously, not to block the UI.
        parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
            // Do your thing, then back to the Main thread
            
            switch result {
            case .success(let feed):
                
                // Grab the parsed feed directly as an optional rss, atom or json feed object
               // feed.rssFeed
                
                // Or alternatively...
                switch feed {
                case let .atom(feed): break       // Atom Syndication Format Feed Model
                case let .rss(feed):
                    DispatchQueue.main.async {
                        // ..and update the UI
                        completionHandler(feed)
                    }
                break        // Really Simple Syndication Feed Model
                case let .json(feed):  break     // JSON Feed Model
                }
                
            case .failure(let error):
                print(error)
            }
            
            
        }
        
    }
    
}
