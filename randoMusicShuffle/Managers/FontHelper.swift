//
//  FontHelper.swift
//  randoMusic
//
//  Created by Andrew Baruk on 7/9/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import UIKit

struct FontHelper {
    
    private static let sFProTextHeavyItalicConstant = "SFProText-HeavyItalic"
    private static let sFProTextLightItalic = "SFProText-LightItalic"
    private static let helveticaNeueMediumItalic = "HelveticaNeue-MediumItalic"
    
    private static let idiom = UIDevice.current.userInterfaceIdiom
    
    private static func sfProHeavyItalicFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: sFProTextHeavyItalicConstant, size: size)!
    }
    
    private static func sFProTextLightItalicFont(withSize size: CGFloat) -> UIFont {
         return UIFont(name: sFProTextLightItalic, size: size)!
     }
    
    static func sfProHeavyItalicFont(forDynamicIdioms dynamic: Bool, andStandardSize size: CGFloat) -> UIFont {
        if dynamic {
        return idiom == UIUserInterfaceIdiom.phone ?
        UIFont(name: sFProTextHeavyItalicConstant, size: size)! :
            UIFont(name: sFProTextHeavyItalicConstant, size: size * 2.0)!
        } else {
            return sfProHeavyItalicFont(withSize: size)
        }
    }
    
    static func sFProTextLightItalicFont(forDynamicIdioms dynamic: Bool, andStandardSize size: CGFloat) -> UIFont {
                if dynamic {
        return idiom == UIUserInterfaceIdiom.phone ?
        UIFont(name: sFProTextLightItalic, size: size)! :
        UIFont(name: sFProTextLightItalic, size: size * 2)!
        } else {
            return sFProTextLightItalicFont(withSize: size)
        }
    }
    
    static func helveticaNeueMediumItalicFont(forDynamicIdioms dynamic: Bool, andStandardSize size: CGFloat) -> UIFont {
                if dynamic {
        return idiom == UIUserInterfaceIdiom.phone ?
        UIFont(name: helveticaNeueMediumItalic, size: size)! :
        UIFont(name: helveticaNeueMediumItalic, size: size * 2)!
        } else {
            return UIFont(name: helveticaNeueMediumItalic, size: size)!
        }
    }
    
}
