//
//  NotificationManager.swift
//  randoMusicShuffle
//
//  Created by Jay on 6/24/20.
//  Copyright © 2020 Andrzej Baruk. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationManager {
    
    static let sharedInstance : NotificationManager = {
        let instance = NotificationManager()
        return instance
    }()
    
    let notificationCenter = UNUserNotificationCenter.current()
    var albums: [AlbumData]? {
        get {
            AlbumDataStore.sharedInstance.getSavedAlbums()
        }
    }
    
    func updateNotifications() {
        
        /// get albums for next month
        let upcomingAlbumAnniversaries = getUpcomingAlbumAnniverasaries()
        
        for album in upcomingAlbumAnniversaries {
            addNotification(album: album)
        }
    }
    
    private func getUpcomingAlbumAnniverasaries() -> [AlbumData] {
        var upcoming = [AlbumData]()
        let currentMonth = Date.init().month
        
        upcoming = albums!.filter({
            if $0.releaseDate == "" { return false }
            let formattedMonth = $0.releaseDate.getFormattedMonth()
            let formattedDay = $0.releaseDate.getFormattedDayNumber()
            return (formattedMonth == currentMonth || formattedMonth == currentMonth.getFormattedNextMonth()) && formattedDay > Int(Date.init().dayNumber)
        }).sorted(by: { $0.releaseDate < $1.releaseDate })
        
        return upcoming
    }
    
    private func addNotification(album: AlbumData) {
        
        let yearDifference = Int(Date.init().year)! - Int(album.releaseDate.getFormattedYear())!
        
        /// setup content
        let content = UNMutableNotificationContent()
        content.title = album.albumTitle
        content.body = "Celebrate the \(yearDifference) Year Anniversary!"
        content.userInfo = ["id" : album.albumPersistentId, "artist" : album.albumArtist]
        content.sound = .default
        
        /// setup the date
        var dateComponents = DateComponents()
        
        dateComponents.calendar = Calendar.current
        dateComponents.month = album.releaseDate.getFormattedMonthNumber()
        dateComponents.year = Int(Date.init().month) == 12 && dateComponents.month! == 1 ? Int(Date.init().year)! + 1 : Int(Date.init().year)!
        dateComponents.day = album.releaseDate.getFormattedDayNumber()
        dateComponents.hour = 10
        dateComponents.minute = 00
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString,
                                            content: content, trigger: trigger)
     //   notificationCenter.removeAllPendingNotificationRequests()
        notificationCenter.getPendingNotificationRequests(completionHandler: { notificationRequests in
            //  print(notificationRequests.first!.content.userInfo["id"]!)
            if (notificationRequests.filter({ $0.content.userInfo["artist"] as! String == album.albumArtist && $0.content.title == album.albumTitle }).count == 0) {
                
                /// schedule the notification
                self.notificationCenter.add(request) { (error) in
                    if error != nil {
                        print("Error occured while adding notification \(error)")
                    }
                }
            }
        })
    }
    
    func printAllNotifications() {
        notificationCenter.getPendingNotificationRequests(completionHandler: { requests in
            print("notifs: \(requests)")
        })
    }
    
    func test() {
               let content = UNMutableNotificationContent()
               content.title = "Weekly Staff Meeting"
               content.body = "Every Tuesday at 2pm"
                content.userInfo = ["id" : "12345"]
        
               // Configure the recurring date.
               var dateComponents = DateComponents()
               dateComponents.calendar = Calendar.current

              // dateComponents.weekday = 3  // Tuesday
               dateComponents.hour = 21    // 14:00 hours
               dateComponents.minute = 05
                  
               // Create the trigger as a repeating event.
               let trigger = UNCalendarNotificationTrigger(
                        dateMatching: dateComponents, repeats: false)
               
               // Create the request
               let uuidString = UUID().uuidString
               let request = UNNotificationRequest(identifier: uuidString,
                           content: content, trigger: trigger)
      
       // notificationCenter.removeAllPendingNotificationRequests()
        
        notificationCenter.getPendingNotificationRequests(completionHandler: { notificationRequests in
         //   print(notificationRequests.first!.content.userInfo["id"]!)
            if (notificationRequests.filter({ $0.content.userInfo["id"] as! String == "" }).count == 0) {
                
            }
        })
        
        
        //  notificationCenter.removeAllPendingNotificationRequests()
        
        /*
         // Schedule the request with the system.
         notificationCenter.add(request) { (error) in
         if error != nil {
         // Handle any errors.
         }
         }
         */
        
        //   notificationCenter.removeAllPendingNotificationRequests()
    }
    
    func removeDeletedAlbumNotification(album: AlbumData) {
        
        notificationCenter.getPendingNotificationRequests(completionHandler: { notificationRequests in

            /// look for existing notification for deleted album and remove if exists
            if let notificationToRemove = notificationRequests.filter({ $0.content.userInfo["id"] as! String == album.albumPersistentId }).first {
                self.notificationCenter.removePendingNotificationRequests(withIdentifiers: [notificationToRemove.identifier])
            }
        })
    }
    
    func getPendingAlbumNotifications(_ completion: @escaping([UNNotificationRequest])->()){
        notificationCenter.getPendingNotificationRequests(completionHandler: { notificationRequests in
         //   print(notificationRequests.first!.content.userInfo["id"]!)
            completion(notificationRequests)
        })
    }
    
}
